package com.fpt.edu.projectthesis;

import com.fpt.edu.projectthesis.entity.Customer;
import com.fpt.edu.projectthesis.service.BookingService;
import com.fpt.edu.projectthesis.service.WorkTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.text.ParseException;

@SpringBootApplication
@EnableScheduling
@Configuration
public class ProjectthesisApplication extends SpringBootServletInitializer {
    public static void main(String[] args) throws ParseException {
        ConfigurableApplicationContext context= SpringApplication.run(ProjectthesisApplication.class, args);
        context.getBean(WorkTableService.class).checkWorktableWhenTurnOnServer();
        context.getBean(WorkTableService.class).hidePastTimeId();
        context.getBean(BookingService.class).cancelPastBooking();
    }

}
