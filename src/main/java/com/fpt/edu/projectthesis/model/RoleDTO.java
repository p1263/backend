package com.fpt.edu.projectthesis.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleDTO {
    private Integer id;
    private String name;
}
