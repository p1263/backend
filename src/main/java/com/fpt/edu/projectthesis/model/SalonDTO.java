package com.fpt.edu.projectthesis.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalonDTO {
    private Integer salonId;
    private String name;
    private String address;
    private Integer managerId;
    private Double latLocation;
    private Double lngLocation;
    private String avatar;
}

