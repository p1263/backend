package com.fpt.edu.projectthesis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SalonWorkDTO {
    private Integer serviceId;
    private String name;
    private Double price;
    private String description;
    private String image;
    private Integer point;
    private boolean status;
}
