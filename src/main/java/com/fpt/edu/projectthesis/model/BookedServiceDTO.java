package com.fpt.edu.projectthesis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookedServiceDTO {
    private Integer id;
    private Integer bookingId;
    private Integer serviceId;
    private String service_name;

    public BookedServiceDTO(Integer bookingId, Integer serviceId) {
        this.bookingId = bookingId;
        this.serviceId = serviceId;
    }
}
