package com.fpt.edu.projectthesis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HistoryDTO {

    private Integer id;
    private Integer customer_id;
    private Integer barber_id;
    private String barber_name;
    private Integer salon_id;
    private String salon_name;
    private Double price;
    private Integer booking_id;
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date date;
    private List<SalonWorkDTO> listService;
    private Integer star;
    private String frontPic;
    private String leftPic;
    private String behindPic;
    private String rightPic;

}
