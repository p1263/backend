package com.fpt.edu.projectthesis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookingDTO {
    private Integer bookingId;
    private Integer customerId;
    private Integer salonId;
    private Integer barberId;
    private Integer timeId;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private Integer star;
    private String status;
    private String frontPic;
    private String leftPic;
    private String behindPic;
    private String rightPic;
    private String customerPhone;
    private String customerName;
    private String barberName;
    private String salonName;
    private String barberAvatar;
    private List<BookedServiceDTO> listBookedService;
    @Temporal(TemporalType.TIME)
    @DateTimeFormat(pattern = "hh:mm:ss")
    private Date timestamp;
    private Double price;
    private String email;

    public BookingDTO(Integer bookingId, Integer customerId, Integer salonId, Integer barberId, Integer timeId, Date date, String status) {
        this.bookingId = bookingId;
        this.customerId = customerId;
        this.salonId = salonId;
        this.barberId = barberId;
        this.timeId = timeId;
        this.date = date;
        this.status = status;
    }
}
