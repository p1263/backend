package com.fpt.edu.projectthesis.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimeWorkTableDTO {
    private Integer timeId;
    private Date timeStamp;

}
