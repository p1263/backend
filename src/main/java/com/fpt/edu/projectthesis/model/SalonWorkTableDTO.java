package com.fpt.edu.projectthesis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SalonWorkTableDTO {
    Integer Id;
    Integer salonId;
    Integer timeId;
    Integer slot;
    Date date;
    private String time;
}
