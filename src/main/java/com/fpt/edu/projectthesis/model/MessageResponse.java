package com.fpt.edu.projectthesis.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageResponse {
    private int status; //00 01 02 03
    private String message;
    private Object data;
}
