package com.fpt.edu.projectthesis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BarberDTO {
    private Integer barberId;
    private String name;
    private String CMND;
    private String email;
    @DateTimeFormat(pattern = "yyyy/MM/dd")
    private Date DOB;
    private String phone;
    private String avatar;
    private Integer salonId;
    private Integer roleId;
    @DateTimeFormat(pattern = "yyyy/DD/mm")
    private Date dayEnter;
    private Double basePoint;
    private Double rankPoint;
    private Integer rankId;
}
