package com.fpt.edu.projectthesis.model;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountDTO {
    private Integer id;
    private String phone;
    private String password;
    private Integer roleId;
    private String deviceToken;
    private String newPassword;
}
