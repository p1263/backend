package com.fpt.edu.projectthesis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BarberWorkTableDTO {
    private Integer id;
    private Integer barberId;
    private Integer bookingId;
    private Integer timeId;
    private Date date;
    private boolean status;
    private Integer salonId;
    private String time;
}
