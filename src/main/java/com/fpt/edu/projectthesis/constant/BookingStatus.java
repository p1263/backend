package com.fpt.edu.projectthesis.constant;

public class BookingStatus {
    public static String WAIT = "Waiting";
    public static String ARRIVED = "Arrived";
    public static String SERVING = "Serving";
    public static String BILLING = "Billing";
    public static String COMPLETED = "Completed";
    public static String CANCELLED = "Cancelled";
}
