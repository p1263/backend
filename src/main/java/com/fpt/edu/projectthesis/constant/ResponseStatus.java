package com.fpt.edu.projectthesis.constant;

public class ResponseStatus {
    public static final int SUCCESS = 0;
    public static final int FAIL = 1;
}
