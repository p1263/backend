package com.fpt.edu.projectthesis.config;

import com.fpt.edu.projectthesis.filter.CustomAuthenticationFilter;
import com.fpt.edu.projectthesis.filter.CustomAuthorizationFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.security.config.http.SessionCreationPolicy.STATELESS;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;
//    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CustomAuthenticationFilter customAuthenticationFilter = new CustomAuthenticationFilter(authenticationManagerBean());
        customAuthenticationFilter.setFilterProcessesUrl("/api/login");
        http.csrf().disable();
        http.sessionManagement().sessionCreationPolicy(STATELESS);
        http.authorizeRequests().antMatchers("/api/login/**", "/api/token/refresh/**", "/api/account/**","/api/otp/**","/api/function/**","/static/**","/images/**","/src/**","/**","/booking/**","/api/home").permitAll(); // What part doesn't secure
        http.authorizeRequests().antMatchers(GET,"/api/customer/**","/api/history/**","/api/service/**","/api/salon/getList").hasAnyAuthority("customer");
        http.authorizeRequests().antMatchers(POST,"/api/barber/salon","/api/customer/**","/api/salon/**","/api/booking/book","/api/service/**","/api/worktable/**").hasAnyAuthority("customer");
//        http.authorizeRequests().antMatchers(GET,"").hasAnyAuthority("barber");
        http.authorizeRequests().antMatchers(POST,"/api/booking/status/finishServe").hasAnyAuthority("barber");
        http.authorizeRequests().antMatchers(GET,"/api/booking/detail/all").hasAnyAuthority("manager");
        http.authorizeRequests().antMatchers(POST,"/api/barber/add","/api/booking/swapBarber").hasAnyAuthority("manager");
//        http.authorizeRequests().antMatchers(GET,"").hasAnyAuthority("admin");
        http.authorizeRequests().antMatchers(POST,"/api/salon/**").hasAnyAuthority("admin");
        http.authorizeRequests().anyRequest().authenticated();
        http.addFilter(customAuthenticationFilter);
        http.addFilterBefore(new CustomAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
