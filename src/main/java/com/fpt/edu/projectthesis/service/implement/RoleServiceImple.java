package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.Role;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.RoleDTO;
import com.fpt.edu.projectthesis.repository.RoleRepository;
import com.fpt.edu.projectthesis.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class RoleServiceImple implements RoleService {

    @Autowired
    RoleRepository roleRepository;

    @Override
    public RoleDTO findRoleById(int id) {
        Object role = roleRepository.getRoleById(id);
        if (role == null) return null;
        if (role instanceof Role) {
            RoleDTO roleDTO = (RoleDTO) role;
            return roleDTO;
        }
        return null;
    }

    @Override
    public List<RoleDTO> getRoleCustomerAndBarber() {
        List<Role> listRole = roleRepository.getRoleCustomerAndBarber();
        List<RoleDTO> listRoleDTO = new ArrayList<>();
        for(Role role : listRole){
            RoleDTO roleDTO = Mapper.toRoleDTO(role);
            listRoleDTO.add(roleDTO);
        }
        return listRoleDTO;
    }
}
