package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.entity.Salon;
import com.fpt.edu.projectthesis.model.SalonDTO;

import java.util.List;

public interface SalonService {
    List<SalonDTO> getListSalon();

    SalonDTO getSalonById(int id);

    String addNewSalon(SalonDTO salonDTO);

    boolean updateSalonInfo(SalonDTO salonDTO);

    List<SalonDTO> getListSalonFree();
}
