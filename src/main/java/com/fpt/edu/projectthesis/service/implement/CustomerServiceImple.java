package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.Customer;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.AdminDTO;
import com.fpt.edu.projectthesis.model.BarberDTO;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import com.fpt.edu.projectthesis.model.ManagerDTO;
import com.fpt.edu.projectthesis.repository.CustomerRepository;
import com.fpt.edu.projectthesis.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
public class CustomerServiceImple implements CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private FileUploadUtilService fileUploadUtil;

    @Override
    public CustomerDTO getCustomerDetailByPhone(String phone) throws ParseException {
        Optional<Customer> customer = customerRepository.findCustomerByPhone(phone);
        if (customer.isPresent()) {
            CustomerDTO customerDTO = Mapper.toCustomerDTO(customer.get());
            if(customerDTO.getName()==null || customerDTO.getName().isEmpty()) customerDTO.setName("");
            if (customerDTO.getDob() == null){
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse("1900-01-01");
                customerDTO.setDob(date);
            }
            return customerDTO;
        }
        return null;
    }

    @Override
    public boolean updateCustomerInfo(CustomerDTO customerDTO) throws ParseException {
        Optional<Customer> customerOptional = customerRepository.findCustomerById(customerDTO.getId());
        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            customer.setName(customerDTO.getName());
            customer.setDob(customerDTO.getDob());
            customer.setEmail(customerDTO.getEmail());
            customer.setAddress(customerDTO.getAddress());
            customer.setAvatar(customerDTO.getAvatar());
            Customer saveCustomer = customerRepository.save(customer);
            if (saveCustomer == null) return false;
            return true;
        }
        return false;
    }

    @Override
    public CustomerDTO getCustomerRankById(int id) {
        return null;
    }

    @Override
    public CustomerDTO getCustomerDetailById(Integer customerId) {
        Optional<Customer> customerOptional = customerRepository.findCustomerById(customerId);
        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            CustomerDTO customerDTO = Mapper.toCustomerDTO(customer);
            return customerDTO;
        }
        return null;
    }

    @Override
    public CustomerDTO insertAvatarToCustomer(String phone, MultipartFile avatar) throws IOException {
        Optional<Customer> customerOptional = customerRepository.findCustomerByPhone(phone);
        if (customerOptional.isPresent()) {
            Customer customer = customerOptional.get();
            String uploadDir = "avatar/" + customer.getPhone();
            String fileName = StringUtils.cleanPath(avatar.getOriginalFilename());
            customer.setAvatar("images/" + uploadDir + "/" + fileName);
            Customer customerResult = customerRepository.save(customer);
            fileUploadUtil.saveFile(uploadDir, fileName, avatar);
            CustomerDTO customerDTO = Mapper.toCustomerDTO(customerResult);
            return customerDTO;
        }
        return null;

    }

}
