package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.RoleDTO;

import java.util.List;

public interface RoleService {
    RoleDTO findRoleById(int id);

    List<RoleDTO> getRoleCustomerAndBarber();
}
