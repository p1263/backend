package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.Manager;
import com.fpt.edu.projectthesis.entity.Salon;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.AccountDTO;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import com.fpt.edu.projectthesis.model.ManagerDTO;
import com.fpt.edu.projectthesis.repository.ManagerRepository;
import com.fpt.edu.projectthesis.repository.SalonRepository;
import com.fpt.edu.projectthesis.service.AccountService;
import com.fpt.edu.projectthesis.service.CustomerService;
import com.fpt.edu.projectthesis.service.FileUploadUtilService;
import com.fpt.edu.projectthesis.service.ManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
public class ManagerServiceImple implements ManagerService {

    public static String imageURL = "https://ironcapbarbershop.com/wp-content/uploads/2019/09/logo1.png";
    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private SalonRepository salonRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private FileUploadUtilService fileUploadUtil;

    @Autowired
    private CustomerService customerService;


    @Override
    public ManagerDTO createManager(ManagerDTO managerDTO) throws ParseException {
        Integer accountRole = accountService.getRoleOfAccountByPhone(managerDTO.getPhone());
        if (accountRole == 1) {
            Optional<Manager> managerOptional = managerRepository.findManagerByPhone(managerDTO.getPhone());
            if (!managerOptional.isPresent()) {
                Manager manager = new Manager();
                manager.setName(managerDTO.getName());
                manager.setCmnd(managerDTO.getCmnd());
                manager.setPhone(managerDTO.getPhone());
                manager.setDob(managerDTO.getDob());
                manager.setAvatar(managerDTO.getAvatar());
                if(managerDTO.getAvatar()== null || managerDTO.getAvatar().isEmpty()) manager.setAvatar(imageURL);
                manager.setSalonId(managerDTO.getSalonId());
                manager.setRoleId(3);
                Manager managerResult = managerRepository.save(manager);
                if (managerResult != null) {
                    CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(managerResult.getPhone());
                    customerDTO.setName(managerResult.getName());
                    customerDTO.setAvatar(managerResult.getAvatar());
                    customerDTO.setDob(managerResult.getDob());
                    boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
                    AccountDTO accountDTO = accountService.getAccountDetailsByPhone(managerResult.getPhone());
                    if (accountDTO != null) {
                        String changeRoleResult = accountService.changeAccountRole(managerDTO.getPhone(), 3);
                        log.info(changeRoleResult.equals("SUCCESS") ? "Create manager Successfully - Change role Successful" : "Create manager Successfully - Change role Fail");
                    }
                    Optional<Salon> salonOptional = salonRepository.findById(managerDTO.getSalonId());
                    if (salonOptional.isPresent()) {
                        Salon salon = salonOptional.get();
                        salon.setManagerId(managerResult.getId());
                        salonRepository.save(salon);
                    } else log.info("Create Manager Successfully - SalonId: {} not exist", managerResult.getSalonId());
                    return Mapper.toManagerDTO(managerResult);
                }
                return null;
            }
            return null;
        }
        return null;
    }

    @Override
    public boolean updateManagerInfo(ManagerDTO managerDTO) throws ParseException {
        Optional<Manager> managerOptional = managerRepository.findById(managerDTO.getId());
        if (managerOptional.isPresent()) {
            Manager manager = managerOptional.get();
            manager.setDob(managerDTO.getDob());
            manager.setCmnd(managerDTO.getCmnd());
            manager.setName(managerDTO.getName());
            manager.setAvatar(managerDTO.getAvatar());
            if(managerDTO.getAvatar()== null || managerDTO.getAvatar().isEmpty()) manager.setAvatar(imageURL);
            Manager managerResult = managerRepository.save(manager);
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(managerResult.getPhone());
            customerDTO.setName(managerResult.getName());
            customerDTO.setAvatar(managerResult.getAvatar());
            customerDTO.setDob(managerResult.getDob());
            boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
            log.info(isSuccess == true ? "Update Manager successfully - Update Customer successfully" : "Update Manager successfully - Update Customer Fail");
            return managerResult == null ? false : true;
        }
        return false;
    }

    @Override
    public ManagerDTO insertAvatarToManager(String phone, MultipartFile avatar) throws IOException, ParseException {
        Optional<Manager> managerOptional = managerRepository.findManagerByPhone(phone);
        if (managerOptional.isPresent()) {
            Manager manager = managerOptional.get();
            String uploadDir = "avatar/" + manager.getPhone();
            String fileName = StringUtils.cleanPath(avatar.getOriginalFilename());
            manager.setAvatar("images/" + uploadDir + "/" + fileName);
            Manager managerResult = managerRepository.save(manager);
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(managerResult.getPhone());
            customerDTO.setName(managerResult.getName());
            customerDTO.setAvatar(managerResult.getAvatar());
            customerDTO.setDob(managerResult.getDob());
            fileUploadUtil.saveFile(uploadDir, fileName, avatar);
            ManagerDTO managerDTO = Mapper.toManagerDTO(managerResult);
            return managerDTO;
        }
        return null;
    }

    @Override
    public ManagerDTO getManagerDetailByPhone(String phone) throws ParseException {
        Optional<Manager> managerOptional = managerRepository.findManagerByPhone(phone);
        if (managerOptional.isPresent()) {
            Manager manager = managerOptional.get();
            if (manager.getDob() == null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse("1900-01-01");
                manager.setDob(date);
            }
            ManagerDTO managerDTO = Mapper.toManagerDTO(manager);
            if(manager.getSalonId() != null){
                String salonName = salonRepository.getSalonNameBySalonId(manager.getSalonId());
                managerDTO.setSalonName(salonName);
            }
            return managerDTO;
        }
        return null;
    }

    @Override
    public ManagerDTO getManagerDetailBySalonId(Integer salonId) throws ParseException {
        Optional<Manager> managerOptional = managerRepository.findManagerBySalonId(salonId);
        if (managerOptional.isPresent()) {
            Manager manager = managerOptional.get();
            if (manager.getDob() == null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse("1900-01-01");
                manager.setDob(date);
            }
            ManagerDTO managerDTO = Mapper.toManagerDTO(manager);
            if(manager.getSalonId() != null){
                String salonName = salonRepository.getSalonNameBySalonId(manager.getSalonId());
                managerDTO.setSalonName(salonName);
            }
            return managerDTO;
        }
        return null;
    }

    @Override
    public ManagerDTO getManagerDetailByManagerId(Integer managerId) throws ParseException {
        Optional<Manager> managerOptional = managerRepository.findById(managerId);
        if (managerOptional.isPresent()) {
            Manager manager = managerOptional.get();
            if (manager.getDob() == null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse("1900-01-01");
                manager.setDob(date);
            }
            ManagerDTO managerDTO = Mapper.toManagerDTO(manager);
            if(manager.getSalonId() != null){
                String salonName = salonRepository.getSalonNameBySalonId(manager.getSalonId());
                managerDTO.setSalonName(salonName);
            }
            return managerDTO;
        }
        return null;
    }


}
