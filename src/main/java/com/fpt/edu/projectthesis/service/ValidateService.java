package com.fpt.edu.projectthesis.service;

public interface ValidateService {

    boolean isValidPhoneAndNotNull(String phone);

    boolean isValidEmail(String email);

    boolean isValidPassword(String password);

    boolean isValidNumber(String inputString);

    boolean isValidPhoneAllowNull(String phone);

    boolean isValidDate(String date);
}
