package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.model.*;
import com.fpt.edu.projectthesis.service.AccountService;
import com.fpt.edu.projectthesis.service.BarberService;
import com.fpt.edu.projectthesis.service.CustomerService;
import com.fpt.edu.projectthesis.service.ManagerService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class FirebaseMessagingService {

    public static String imageUrl = "http://20.205.212.39:3000/images/app-icons/1/img.png";

    @Autowired
    private FirebaseMessaging firebaseMessaging;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private BarberService barberService;

    public String sendNotification(Note note, String token) throws FirebaseMessagingException {
        Notification notification = Notification.builder()
                .setTitle(note.getSubject())
                .setBody(note.getContent())
                .build();

        Message message = Message.builder().setToken(token)
                .setNotification(notification)
                .putAllData(note.getData())
                .build();
        return firebaseMessaging.send(message);
    }

    public void sendNotiForManager(BookingDTO bookingDTO, String subject, String content) throws ParseException, FirebaseMessagingException {
        ManagerDTO managerDTO = managerService.getManagerDetailBySalonId(bookingDTO.getSalonId());
        String managerToken = accountService.getAccountToken(managerDTO.getPhone());
        if (managerToken == null || managerToken.isEmpty()) {
            log.info("Send Notification FAIL: Manager {} has no device token", managerDTO.getPhone());
        } else {
            Note note = new Note();
            note.setImage(imageUrl);
            note.setSubject(subject);
            note.setContent(content);
            Map<String, String> data = new HashMap<>();
            data.put("bookingId", String.valueOf(bookingDTO.getBookingId()));
            note.setData(data);
            String messsage = sendNotification(note,managerToken);
            log.info(messsage);
        }
    }

    public void sendNotiForBarber(BookingDTO bookingDTO, String subject, String content) throws FirebaseMessagingException {
        BarberDTO barberDTO = barberService.getBarberByBarberId(bookingDTO.getBarberId());
        String barberToken = accountService.getAccountToken(barberDTO.getPhone());
        if (barberToken == null || barberToken.isEmpty()) {
            log.info("Send Notification FAIL: Barber {} has no device token", barberDTO.getPhone());
        } else {
            Note note = new Note();
            note.setImage(imageUrl);
            note.setSubject(subject);
            note.setContent(content);
            Map<String, String> data = new HashMap<>();
            data.put("bookingId", String.valueOf(bookingDTO.getBookingId()));
            note.setData(data);
            String message = sendNotification(note,barberToken);
            log.info(message);
        }
    }

    public void sendNotiForCustomer(BookingDTO bookingDTO, String subject, String content) throws FirebaseMessagingException {
        CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
        String customerToken = accountService.getAccountToken(customerDTO.getPhone());
        if (customerToken == null || customerToken.isEmpty()) {
            log.info("Send Notification FAIL: Customer {} has no device token", customerDTO.getPhone());
        } else {
            Note note = new Note();
            note.setImage(imageUrl);
            note.setSubject(subject);
            note.setContent(content);
            Map<String, String> data = new HashMap<>();
            data.put("bookingId", String.valueOf(bookingDTO.getBookingId()));
            note.setData(data);
            String message= sendNotification(note,customerToken);
            log.info(message);
        }

    }
}
