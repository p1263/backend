package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.entity.Barber;
import com.fpt.edu.projectthesis.entity.BarberWorkTable;
import com.fpt.edu.projectthesis.entity.Salon;
import com.fpt.edu.projectthesis.entity.SalonWorkTable;
import com.fpt.edu.projectthesis.model.BarberWorkTableDTO;
import com.fpt.edu.projectthesis.model.SalonWorkTableDTO;

import java.text.ParseException;
import java.util.List;

public interface WorkTableService {

    void insertNewBarberWorktable(Barber barber);


    List<BarberWorkTableDTO> getWorkTablesByBarberId(Integer barberId, Integer roleId) throws ParseException;

    List<SalonWorkTableDTO> getWorkTablesBySalonId(Integer salonId, Integer roleId) throws ParseException;

    void checkWorktableWhenTurnOnServer();

    void insertNewSalonWorktable(Salon salon);

    void hidePastTimeId() throws ParseException;
}
