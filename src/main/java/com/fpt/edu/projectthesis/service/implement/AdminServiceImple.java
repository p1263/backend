package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.Admin;
import com.fpt.edu.projectthesis.entity.Customer;
import com.fpt.edu.projectthesis.entity.Salon;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.AccountDTO;
import com.fpt.edu.projectthesis.model.AdminDTO;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import com.fpt.edu.projectthesis.repository.AdminRepository;
import com.fpt.edu.projectthesis.service.AccountService;
import com.fpt.edu.projectthesis.service.AdminService;
import com.fpt.edu.projectthesis.service.CustomerService;
import com.fpt.edu.projectthesis.service.FileUploadUtilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
@Slf4j
public class AdminServiceImple implements AdminService {

    public static String imageURL = "https://ironcapbarbershop.com/wp-content/uploads/2019/09/logo1.png";

    @Autowired
    private FileUploadUtilService fileUploadUtil;

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CustomerService customerService;

    @Override
    public AdminDTO createAdmin(AdminDTO adminDTO) throws ParseException {
        Optional<Admin> adminOptional = adminRepository.findAdminByPhone(adminDTO.getPhone());
        if (!adminOptional.isPresent()) {
            Admin admin = new Admin();
            admin.setName(adminDTO.getName());
            admin.setPhone(adminDTO.getPhone());
            admin.setCmnd(adminDTO.getCmnd());
            admin.setDob(adminDTO.getDob());
            admin.setAvatar(adminDTO.getAvatar());
            if(adminDTO.getAvatar()==null || adminDTO.getAvatar().isEmpty()) admin.setAvatar(imageURL);
            admin.setRoleId(4);
            Admin adminResult = adminRepository.save(admin);
            if (adminResult != null) {
                CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(adminResult.getPhone());
                customerDTO.setName(adminResult.getName());
                customerDTO.setAvatar(adminResult.getAvatar());
                customerDTO.setDob(adminResult.getDob());
                boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
                AccountDTO accountDTO = accountService.getAccountDetailsByPhone(adminResult.getPhone());
                if (accountDTO != null) {
                    String changeRoleResult = accountService.changeAccountRole(adminDTO.getPhone(), 4);
                    log.info(changeRoleResult.equals("SUCCESS") ? "Create Admin Successfully - Change role Successful" : "Create manager Successfully - Change role Fail");
                    return Mapper.toAdminDTO(adminResult);
                }
                log.info("Create Admin FAIL - Account not exist");
                return null;
            }
            log.info("Create Admin FAIL - Save info FAIL");
            return null;
        }
        return null;
    }

    @Override
    public AdminDTO getInfoAdminByPhone(String phone) throws ParseException {
        Optional<Admin> adminOptional = adminRepository.findAdminByPhone(phone);
        if (adminOptional.isPresent()) {
            Admin admin = adminOptional.get();
            if (admin.getDob() == null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                Date date = formatter.parse("1900-01-01");
                admin.setDob(date);
            }
            return Mapper.toAdminDTO(admin);
        }
        return null;

    }

    @Override
    public AdminDTO updateInfoAdmin(AdminDTO adminDTO) throws ParseException {
        Optional<Admin> adminOptional = adminRepository.findById(adminDTO.getId());
        if (adminOptional.isPresent()) {
            Admin admin = adminOptional.get();
            admin.setName(adminDTO.getName());
            admin.setCmnd(adminDTO.getCmnd());
            admin.setDob(adminDTO.getDob());
            admin.setAvatar(adminDTO.getAvatar());
            if(adminDTO.getAvatar()==null || adminDTO.getAvatar().isEmpty()) admin.setAvatar(imageURL);
            Admin adminResult = adminRepository.save(admin);
            //change info of customer
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(adminResult.getPhone());
            customerDTO.setName(adminResult.getName());
            customerDTO.setAvatar(adminResult.getAvatar());
            customerDTO.setDob(adminResult.getDob());
            boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
            log.info(isSuccess == true ? "Update admin successfully - Update Customer successfully" : "Update admin successfully - Update Customer Fail");
            return adminResult == null ? null : Mapper.toAdminDTO(adminResult);
        }
        return null;
    }

    @Override
    public AdminDTO insertAvatarToAdmin(String phone, MultipartFile avatar) throws IOException, ParseException {
        Optional<Admin> adminOptional = adminRepository.findAdminByPhone(phone);
        if (adminOptional.isPresent()) {
            Admin admin = adminOptional.get();
            String uploadDir = "avatar/" + admin.getPhone();
            String fileName = StringUtils.cleanPath(avatar.getOriginalFilename());
            admin.setAvatar("images/" + uploadDir + "/" + fileName);
            Admin adminResult = adminRepository.save(admin);
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(adminResult.getPhone());
            customerDTO.setName(adminResult.getName());
            customerDTO.setAvatar(adminResult.getAvatar());
            customerDTO.setDob(adminResult.getDob());
            boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
            fileUploadUtil.saveFile(uploadDir, fileName, avatar);
            AdminDTO adminDTO = Mapper.toAdminDTO(adminResult);
            return adminDTO;
        }
        return null;
    }

    @Override
    public AdminDTO getInfoAdminById(Integer id) {
        Optional<Admin> adminOptional = adminRepository.findById(id);
        if (adminOptional.isPresent()) {
            Admin admin = adminOptional.get();
            return Mapper.toAdminDTO(admin);
        }
        return null;
    }
}
