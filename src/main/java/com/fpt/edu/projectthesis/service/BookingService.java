package com.fpt.edu.projectthesis.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fpt.edu.projectthesis.entity.Barber;
import com.fpt.edu.projectthesis.entity.Booking;
import com.fpt.edu.projectthesis.entity.Customer;
import com.fpt.edu.projectthesis.model.BarberDTO;
import com.fpt.edu.projectthesis.model.BookingDTO;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface BookingService {

    Object insertNewBooking(BookingDTO bookingDTO,int role) throws ParseException, FirebaseMessagingException;



    BookingDTO getBookingDTODetailByBookingId(Integer bookingId);

    Object getAllListBookingByDateAndPhone(Integer salonId, String phone) throws ParseException;

    String cancelBooking(Integer bookingId) throws ParseException, FirebaseMessagingException;

    Object getBookedByCustomerId(Integer customerId);

    BookingDTO insertImagesToBooking(Integer bookingId, String frontPic, String leftPic, String behindPic, String rightPic) throws IOException, FirebaseMessagingException, ParseException;

    BookingDTO getServiceName(BookingDTO bookingDTO);

    boolean confirmCustomerArrived(Integer bookingId, Integer barberId, JsonNode services) throws FirebaseMessagingException, ParseException;

    BookingDTO confirmComplete(Integer bookingId) throws ParseException, FirebaseMessagingException;

    boolean confirmServingCustomer(Integer bookingId);

    Object managerBooking(BookingDTO bookingDTO, Customer customer) throws ParseException, FirebaseMessagingException;

    Object managerBookingExistedCustomer(BookingDTO bookingDTO) throws ParseException, FirebaseMessagingException;

    void cancelPastBooking();

    List<BookingDTO> getAllListBillingOfSalon(Integer salonId);

    List<BookingDTO> getListWorkOfDayForBarber( Integer barberId, int page, String status);

    Object getAllListBookingBySalonIdAndStatus(Integer salonId, String status,int page);

    String customerRating(Integer bookingId, Integer rate);
}
