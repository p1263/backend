package com.fpt.edu.projectthesis.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileUploadUtilService {

    public void saveFile(String uploadDir, String fileName, MultipartFile multipartFile) throws IOException;
}
