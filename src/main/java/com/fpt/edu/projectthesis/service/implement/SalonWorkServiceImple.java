package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.SalonWork;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.SalonWorkDTO;
import com.fpt.edu.projectthesis.repository.SalonWorkRepository;
import com.fpt.edu.projectthesis.service.FileUploadUtilService;
import com.fpt.edu.projectthesis.service.SalonWorkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SalonWorkServiceImple implements SalonWorkService {

    @Autowired
    private SalonWorkRepository salonWorkRepository;

    @Autowired
    private FileUploadUtilService fileUploadUtil;

    @Override
    public List<SalonWorkDTO> getAllSalonWork() {
        List<SalonWork> listSalonWork = salonWorkRepository.findAllSalonWorkByStatusTrue();
        List<SalonWorkDTO> listSalonWorkDTO = new ArrayList<>();
        for (SalonWork salon : listSalonWork) {
            SalonWorkDTO salonWorkDTO = Mapper.toSalonWorkDTO(salon);
            listSalonWorkDTO.add(salonWorkDTO);
        }
        return listSalonWorkDTO;
    }

    @Override
    public SalonWorkDTO getSalonWorkById(Integer id) {
        Optional<SalonWork> salonWorkOptional = salonWorkRepository.findSalonWorkByServiceId(id);
        if (salonWorkOptional.isPresent()) {
            SalonWorkDTO salonWorkDTO = Mapper.toSalonWorkDTO(salonWorkOptional.get());
            return salonWorkDTO;
        }
        return null;
    }

    @Override
    public SalonWorkDTO createSalonService(SalonWorkDTO salonWorkDTO) {
        SalonWork salonWork = new SalonWork();
        salonWork.setName(salonWorkDTO.getName());
        salonWork.setDescription(salonWorkDTO.getDescription());
        salonWork.setPrice(salonWorkDTO.getPrice());
        salonWork.setPoint(salonWorkDTO.getPoint());
        salonWork.setImage(salonWorkDTO.getImage());
        salonWork.setStatus(true);
        SalonWork salonWorkResult = salonWorkRepository.save(salonWork);
        if (salonWorkResult != null) {
            return Mapper.toSalonWorkDTO(salonWorkResult);
        }
        return null;
    }

    @Override
    public SalonWorkDTO updateSalonService(SalonWorkDTO salonWorkDTO) {
        Optional<SalonWork> salonWorkOptional = salonWorkRepository.findById(salonWorkDTO.getServiceId());
        if (salonWorkOptional.isPresent()) {
            SalonWork salonWork = salonWorkOptional.get();
            salonWork.setName(salonWorkDTO.getName());
            salonWork.setDescription(salonWorkDTO.getDescription());
            salonWork.setPrice(salonWorkDTO.getPrice());
            salonWork.setPoint(salonWorkDTO.getPoint());
            SalonWork salonWorkResult = salonWorkRepository.save(salonWork);
            if (salonWorkResult != null) {
                return Mapper.toSalonWorkDTO(salonWork);
            }
        }
        return null;
    }

    @Override
    public boolean deleteSalonService(Integer salonServiceId) {
        Optional<SalonWork> salonWorkOptional = salonWorkRepository.findById(salonServiceId);
        if (salonWorkOptional.isPresent()) {
            Integer result = salonWorkRepository.deleteSalonWorkByServicesId(salonServiceId);
            if (result > 0) return true;
            return false;
        }
        return false;
    }
}
