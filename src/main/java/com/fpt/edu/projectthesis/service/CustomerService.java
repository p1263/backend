package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.BookingDTO;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

public interface CustomerService {

    CustomerDTO getCustomerDetailByPhone(String phone) throws ParseException;

    boolean updateCustomerInfo(CustomerDTO customerDTO) throws ParseException;

    CustomerDTO getCustomerRankById(int customerId);

    CustomerDTO getCustomerDetailById(Integer customerId);

    CustomerDTO insertAvatarToCustomer(String phone, MultipartFile avatar) throws IOException;


}
