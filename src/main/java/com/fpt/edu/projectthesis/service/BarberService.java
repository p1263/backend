package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.BarberDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

public interface BarberService {
    List<BarberDTO> getBarberBySalonId(Integer salonId);

    BarberDTO getBarberByBarberId(Integer barberId);

    List<BarberDTO> getAllBarbers();

    List<Integer> getAllBarberId();

    String addNewBarber(BarberDTO barberDTO) throws ParseException;

    String updateBarberPointByService(Integer barberId, Integer customerId, List<Integer> listService);

    String updateBarberPointByRating(Integer barberId, Integer customerId, Integer star);

    BarberDTO insertAvatartoBarber(String phone, MultipartFile avatar) throws IOException, ParseException;

    void resetRandomList(Integer salonId);

    BarberDTO getBarberByPhone(String phone);

    BarberDTO updateBarberInfo(BarberDTO barberDTO) throws ParseException;

    List<BarberDTO> getListBarberFreeOfSlot(Integer salonId, Integer timeId, Date date);
}
