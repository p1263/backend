package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.RoleFunction;
import com.fpt.edu.projectthesis.repository.RoleFunctionRepository;
import com.fpt.edu.projectthesis.service.RoleFunctionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleFunctionServiceImple implements RoleFunctionService {

    @Autowired
    private RoleFunctionRepository roleFunctionRepository;

    @Override
    public List<RoleFunction> getAllFunctionOfRole(Integer roleId) {
        List<RoleFunction> listRoleFunction = roleFunctionRepository.findAllByRoleId(roleId);
        if(listRoleFunction != null) return listRoleFunction;
        return null;
    }
}
