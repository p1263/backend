package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.*;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.BarberDTO;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import com.fpt.edu.projectthesis.repository.*;
import com.fpt.edu.projectthesis.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


@Service
@Slf4j
public class BarberServiceImple implements BarberService {

    public static String imageURL = "https://ironcapbarbershop.com/wp-content/uploads/2019/09/logo1.png";

    @Autowired
    private BarberWorkTableRepository barberWorkTableRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private BarberRepository barberRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private WorkTableService workTableService;

    @Autowired
    private SalonWorkService salonWorkService;

    @Autowired
    private CustomerRankRepository customerRankRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private RateRepository rateRepository;

    @Autowired
    private SalonWorkTableRepository salonWorkTableRepository;

    @Autowired
    private CustomerAssignedRepository customerAssignedRepository;

    @Autowired
    private FileUploadUtilService fileUploadUtil;

    @Autowired
    private BarberRankRepository barberRankRepository;



    @Override
    public List<BarberDTO> getBarberBySalonId(Integer salonId) {

        List<Barber> barbers = barberRepository.getBarbersBySalon_id(salonId);
        List<BarberDTO> listBarber= new ArrayList<>();
        for(Barber barber: barbers){
            listBarber.add(Mapper.toBarberDTO(barber));
        }
        return listBarber;

    }

    @Override
    public BarberDTO getBarberByBarberId(Integer barberId) {
        Barber barber = barberRepository.getBarberByBarber_id(barberId);
        return Mapper.toBarberDTO(barber);
    }

    @Override
    public List<BarberDTO> getAllBarbers() {
        List<Barber> barbers = barberRepository.getAllBarber();
        List<BarberDTO> listBarber= new ArrayList<>();
        for(Barber barber: barbers){
            listBarber.add(Mapper.toBarberDTO(barber));
        }
        return listBarber;
    }

    @Override
    public List<Integer> getAllBarberId() {
        return barberRepository.getAllBarberId();
    }

    @Override
    public String addNewBarber(BarberDTO barberDTO) throws ParseException {
        if(barberRepository.getBarberByPhone(barberDTO.getPhone()) == null){
            if(accountService.getRoleOfAccountByPhone(barberDTO.getPhone())>2) return "0";
            Barber barber = Mapper.toBarber(barberDTO);
            if(barberDTO.getAvatar()==null || barberDTO.getAvatar().isEmpty()) barber.setAvatar(imageURL);
            barber.setRoleId(barberDTO.getRoleId());
            barberRepository.save(barber);
            //update customer info
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(barber.getPhone());
            customerDTO.setName(barber.getName());
            customerDTO.setAvatar(barber.getAvatar());
            customerDTO.setDob(barber.getDOB());
            boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
            //
            workTableService.insertNewBarberWorktable(barber);
            List<SalonWorkTable> salonWorkTables = salonWorkTableRepository.getSalonWorkTableBySalonId(barber.getSalonId());
            for (SalonWorkTable workTable: salonWorkTables) {
                workTable.setSlot(workTable.getSlot()+1);
                salonWorkTableRepository.save(workTable);
            }
            CustomerAssigned customerAssigned = new CustomerAssigned();
            customerAssigned.setNoca(2);
            customerAssigned.setSalonId(barber.getSalonId());
            customerAssigned.setBarberId(barber.getBarberId());
            customerAssignedRepository.save(customerAssigned);
            workTableService.hidePastTimeId();
            //Change role of Account
            if(accountService.isAccountExist(barberDTO.getPhone())){
                String result = accountService.changeAccountRole(barberDTO.getPhone(), 2);
                log.info((result == "SUCCESS" ? "Đổi role: {} thành công" : "Đổi role {} không thành công"),barberDTO.getPhone());
            }
            return "Thêm mới barber thành công";
        }
        return null;

    }

    @Override
    public String updateBarberPointByService(Integer barberId, Integer customerId, List<Integer> listServices) {
        Barber barber = barberRepository.getBarberByBarber_id(barberId);
        CustomerRank customerRank = customerRankRepository.getRankById(customerRepository.getCustomerRankById(customerId));
        BarberRank barberRank = barberRankRepository.getBarberRankByBarberId(barber.getRankId());
        double sum=0;
        for(int i : listServices){
            sum= sum +customerRank.getMultiplier() * salonWorkService.getSalonWorkById(i).getPoint() * barberRank.getMultiplier();
        }
        barber.setRankPoint(barber.getRankPoint()+sum);
        barberRepository.save(barber);
        updateBarberRank(barberId);
        return "Cập nhật điểm barber thành công";
    }

    @Override
    public String updateBarberPointByRating(Integer barberId, Integer customerId, Integer star) {
        Barber barber = barberRepository.getBarberByBarber_id(barberId);
        CustomerRank customerRank = customerRankRepository.getRankById(customerRepository.getCustomerRankById(customerId));
        BarberRank barberRank = barberRankRepository.getBarberRankByBarberId(barber.getRankId());
        Rate rate = rateRepository.getRateById(star);
        double sum=customerRank.getMultiplier()*rate.getPoint()*barberRank.getMultiplier();
        barber.setRankPoint(barber.getRankPoint()+sum);
        barberRepository.save(barber);
        updateBarberRank(barberId);
        return "Cập nhật điểm barber thành công";
    }

    @Override
    public void resetRandomList(Integer salonId) {
        if(customerAssignedRepository.getSumRandomList(salonId)==0){
            List<CustomerAssigned> list = customerAssignedRepository.getListbySalonId(salonId);
            for(CustomerAssigned c : list){
                c.setNoca(barberRankRepository.getRankById(barberRepository.getBarberByBarber_id(c.getBarberId()).getRankId()));
                customerAssignedRepository.save(c);
            }
        }
    }

    @Override
    public BarberDTO getBarberByPhone(String phone) {
        Barber barber = barberRepository.getBarberByPhone(phone);
        if(barber==null) return null;
        return Mapper.toBarberDTO(barber);

    }

    @Override
    public BarberDTO updateBarberInfo(BarberDTO barberDTO) throws ParseException {
        Optional<Barber> barberOptional = barberRepository.findById(barberDTO.getBarberId());
        if(barberOptional.isPresent()){
            Barber barber = barberOptional.get();
            barber.setName(barberDTO.getName());
            barber.setCMND(barberDTO.getCMND());
            barber.setEmail(barberDTO.getEmail());
            barber.setDOB(barberDTO.getDOB());
            barber.setAvatar(barberDTO.getAvatar());
            if(barberDTO.getAvatar()==null || barberDTO.getAvatar().isEmpty()) barber.setAvatar(imageURL);
            Barber barberResult = barberRepository.save(barber);
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(barberResult.getPhone());
            customerDTO.setName(barberResult.getName());
            customerDTO.setAvatar(barberResult.getAvatar());
            customerDTO.setDob(barberResult.getDOB());
            boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
            log.info(isSuccess == true ? "Update Barber successfully - Update Customer successfully" : "Update Barber successfully - Update Customer Fail");
            return Mapper.toBarberDTO(barberResult);
        }
        return null;
    }

    @Override
    public List<BarberDTO> getListBarberFreeOfSlot(Integer salonId, Integer timeId, Date date) {
        List<BarberDTO> listBarberDTO = new ArrayList<>();
        List<Integer> listBarberFree = barberWorkTableRepository.getFreeBarberId(salonId, timeId, date);
        for(Integer barberId: listBarberFree){
            Optional<Barber> barberOptional = barberRepository.findById(barberId);
            if(barberOptional.isPresent()){
                BarberDTO barberDTO = Mapper.toBarberDTO(barberOptional.get());
                listBarberDTO.add(barberDTO);
            }
        }
        return listBarberDTO;
    }

    @Override
    public BarberDTO insertAvatartoBarber(String phone, MultipartFile avatar) throws IOException, ParseException {
        Optional<Barber> barberOptional = barberRepository.findBarberByPhone(phone);
        if(barberOptional.isPresent()){
            Barber barber = barberOptional.get();
            String uploadDir = "avatar/" + barber.getPhone();
            String fileName = StringUtils.cleanPath(avatar.getOriginalFilename());
            barber.setAvatar("images/"+uploadDir+"/"+fileName);
            Barber barberResult = barberRepository.save(barber);
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(barberResult.getPhone());
            customerDTO.setName(barberResult.getName());
            customerDTO.setAvatar(barberResult.getAvatar());
            customerDTO.setDob(barberResult.getDOB());
            boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
            fileUploadUtil.saveFile(uploadDir,fileName,avatar);
            BarberDTO barberDTO = Mapper.toBarberDTO(barberResult);
            return barberDTO;
        }
        return  null;
    }


    public void updateBarberRank(Integer Id) {
        Double rankPoint = barberRepository.getBarberRankPointById(Id);
        Barber barber = barberRepository.getBarberByBarber_id(Id);
        int oldRank = barber.getRankId();
        if(rankPoint < 400) barber.setRankId(5);
        else if (rankPoint >=400 && rankPoint<600) barber.setRankId(4);
        else if(rankPoint >=600 && rankPoint<800) barber.setRankId(3);
        else if (rankPoint>=800 && rankPoint<1000) barber.setRankId(2);
        else if(rankPoint>=1000) barber.setRankId(1);
        barber.setBasePoint(barberRankRepository.getBarberBaseRank(barber.getRankId()));
        if(oldRank>barber.getRankId()){
            CustomerAssigned c = customerAssignedRepository.getByBarberId(Id);
            c.setNoca(c.getNoca()+1);
            customerAssignedRepository.save(c);
        }else if(oldRank<barber.getRankId()){
            CustomerAssigned c = customerAssignedRepository.getByBarberId(Id);
            if(c.getNoca()>0) c.setNoca(c.getNoca()-1);
            customerAssignedRepository.save(c);
        }
        barberRepository.save(barber);

    }
    @Scheduled(cron = "0 2 0 * * ?")
    public void resetBarberPoint(){
        TimeUnit time = TimeUnit.DAYS;
        List<Barber> barbers = barberRepository.getAllBarber();
        for(Barber barber : barbers){
            Date today = new Date();
            long diff = today.getTime() - barber.getDayEnter().getTime();
            long difference = time.convert(diff, TimeUnit.MILLISECONDS);
            double plus = 0;
            if(difference<=365) plus=50;
            else if( difference<=732) plus=100;
            else if( difference<=1825) plus=200;
            else plus=500;
            barber.setRankPoint(barber.getBasePoint()+plus);
        }
    }
}
