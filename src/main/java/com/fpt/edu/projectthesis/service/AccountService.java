package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.AccountDTO;
import org.springframework.stereotype.Service;

import java.text.ParseException;


public interface AccountService {


    boolean createAccount(AccountDTO accountDTO) throws ParseException;

    AccountDTO getAccountDetailsByPhone(String phone);

    boolean isAccountExist(String phone);

    boolean changePassword(String password, String phone);

    String changeAccountRole(String phone, int role);

    Integer getRoleOfAccountByPhone(String phone);

    void insertDeviceTokenToAccount(String phone, String token);

    String getAccountToken(String phone);

    String changeNewPassword(Integer accountId, String oldPwd, String newPwd);

    boolean logOut(String phone);
}
