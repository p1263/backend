package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.Salon;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.SalonDTO;
import com.fpt.edu.projectthesis.repository.SalonRepository;
import com.fpt.edu.projectthesis.service.SalonService;
import com.fpt.edu.projectthesis.service.WorkTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class SalonServiceImple implements SalonService {

    public static String imageURL = "https://ironcapbarbershop.com/wp-content/uploads/2019/09/logo1.png";

    @Autowired
    private SalonRepository salonRepository;

    @Autowired
    private WorkTableService workTableService;

    @Override
    public List<SalonDTO> getListSalon() {

        List<Salon> salons = salonRepository.getAllSalon();
        List<SalonDTO> salonDTOs = new ArrayList<>();
        for (Salon salon : salons) {
            salonDTOs.add(Mapper.toSalonDTO(salon));
        }

        return salonDTOs;
    }

    @Override
    public SalonDTO getSalonById(int id) {
        Optional<Salon> salon = salonRepository.findById(id);
        if (salon.isPresent()) {
            return Mapper.toSalonDTO(salon.get());
        }
        return null;
    }

    @Override
    public String addNewSalon(SalonDTO salonDTO) {
        if (salonRepository.getSalonByLocation(salonDTO.getLatLocation(), salonDTO.getLngLocation()).size() != 0) {
            return " Salon đã tồn tại";
        } else {
            Salon salon = Mapper.toSalon(salonDTO);
            salonRepository.save(salon);
            workTableService.insertNewSalonWorktable(salon);
            return " Tạo salon mới thành công";
        }
    }

    @Override
    public boolean updateSalonInfo(SalonDTO salonDTO) {
        Optional<Salon> salonOptional = salonRepository.findById(salonDTO.getSalonId());
        if (salonOptional.isPresent()) {
            Salon salon = salonOptional.get();
            salon.setName(salonDTO.getName());
            salon.setAddress(salonDTO.getAddress());
            if (!salonDTO.getAvatar().isEmpty() && salonDTO != null) {
                salon.setAvatar(salonDTO.getAvatar());
            }else{
                salon.setAvatar(imageURL);
            }
            salon.setLatLocation(salonDTO.getLatLocation());
            salon.setLngLocation(salonDTO.getLngLocation());
            salon.setManagerId(salonDTO.getManagerId());
            Salon salonResult = salonRepository.save(salon);
            if (salonResult != null) return true;
            return false;
        }
        return false;
    }

    @Override
    public List<SalonDTO> getListSalonFree() {
        List<Salon> listSalon = salonRepository.getListSalonFree();
        if (listSalon != null && !listSalon.isEmpty()) {
            List<SalonDTO> listSalonDTO = new ArrayList<>();
            for (Salon salon : listSalon) {
                SalonDTO salonDTO = Mapper.toSalonDTO(salon);
                listSalonDTO.add(salonDTO);
            }
            return listSalonDTO;
        }
        return null;
    }

}
