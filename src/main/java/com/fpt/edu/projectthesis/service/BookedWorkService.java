package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.BookedServiceDTO;

import java.util.List;

public interface BookedWorkService {

    void addServiceToBooking(BookedServiceDTO bookedServiceDTO);

    List<BookedServiceDTO> getListServiceByBookingId(Integer bookingId);

    void deleteListServiceByBookingId(Integer bookingId);

    void deleteBookedServiceByBookingIdAndServiceId(Integer bookingId, Integer bookedServiceId);
}
