package com.fpt.edu.projectthesis.service.implement;

import com.fasterxml.jackson.databind.JsonNode;
import com.fpt.edu.projectthesis.constant.BookingStatus;
import com.fpt.edu.projectthesis.entity.*;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.*;
import com.fpt.edu.projectthesis.model.BarberDTO;
import com.fpt.edu.projectthesis.model.BookingDTO;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import com.fpt.edu.projectthesis.repository.*;
import com.fpt.edu.projectthesis.service.*;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class BookingServiceImple implements BookingService {


    @Autowired
    private HistoryService historyService;

    @Autowired
    private SalonWorkService salonWorkService;

    @Autowired
    private FirebaseMessagingService firebaseMessagingService;

    @Autowired
    private BookingRepository bookingRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private SalonService salonService;

    @Autowired
    private BarberService barberService;

    @Autowired
    private ManagerService managerService;

    @Autowired
    private BookedWorkService bookedWorkService;

    @Autowired
    private BarberWorkTableRepository barberWorkTableRepository;

    @Autowired
    private CustomerAssignedRepository customerAssignedRepository;

    @Autowired
    private SalonWorkTableRepository salonWorkTableRepository;

    @Autowired
    private TimeWorkTableRepository timeWorkTableRepository;

    @Autowired
    private CustomerRepository customerRepository;

    private final SimpleDateFormat dt1 = new SimpleDateFormat("dd-MM-yyyy");
    private final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    @Override
    public Object insertNewBooking(BookingDTO bookingDTO, int role) throws ParseException, FirebaseMessagingException {
        barberService.resetRandomList(bookingDTO.getSalonId());
        Booking pre = bookingRepository.getBookedByCustomerId(bookingDTO.getCustomerId());
        if (pre != null) {
            BookingDTO bookingDTO1 = Mapper.toBookingDTO(pre);
            bookingDTO1.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO1.getTimeId()));
            bookingDTO1.setSalonName(salonService.getSalonById(bookingDTO1.getSalonId()).getName());
            String time = bookingDTO1.getTimestamp().toString();
            return "Bạn đã đặt lịch vào lúc " + time.substring(0, 5) + " ngày " + dt1.format(bookingDTO1.getDate()) + " tại " + bookingDTO1.getSalonName();
        }
        Booking booking = Mapper.toBooking(bookingDTO);
        booking.setStatus(BookingStatus.WAIT);
        if (booking.getBarberId() == null) {
            int barber = bookRandom(booking.getSalonId(), booking.getTimeId(), booking.getDate(), role);
            if (barber == 0) return "Không có barber nào trống lịch";
            booking.setBarberId(barber);
        }
        Date time = timeWorkTableRepository.getTimebyId(booking.getTimeId());
        Date day = dateFormat.parse(dateFormat.format(Calendar.getInstance().getTime()));
        Date now = formatter.parse(formatter.format(Calendar.getInstance().getTime()));
        BarberWorkTable b = barberWorkTableRepository.getBarberWorkTableToUpdate(booking.getBarberId(), booking.getTimeId(), booking.getDate());
        SalonWorkTable s = new SalonWorkTable();
        if(day.compareTo(booking.getDate())==0 && time.compareTo(now)<0 && role==3){
            if(b.getBookingId()==null){
                b.setBookingId(booking.getBookingId());
            }else return "Barber đã có lịch cắt";
        }else{
            if (b.isStatus()) {
                b.setBookingId(booking.getBookingId());
                b.setStatus(false);
            } else return "Barber đã có lịch cắt";
            s = salonWorkTableRepository.getSalonWorkTableToUpdate(booking.getSalonId(), booking.getTimeId(), booking.getDate());
            if (s.getSlot() > 0) {
                s.setSlot(s.getSlot() - 1);

            } else return "Salon không còn lịch trống trong khung giờ này";
        }
        barberWorkTableRepository.save(b);
        salonWorkTableRepository.save(s);
        CustomerAssigned c = customerAssignedRepository.getByBarberId(booking.getBarberId());
        if (c.getNoca() > 0) c.setNoca(c.getNoca() - 1);
        customerAssignedRepository.save(c);
        booking = bookingRepository.save(booking);
        for (BookedServiceDTO service : bookingDTO.getListBookedService()) {
            service.setBookingId(booking.getBookingId());
            bookedWorkService.addServiceToBooking(service);
        }
        BookingDTO bookingDTO1 = Mapper.toBookingDTO(booking);
        CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
        bookingDTO1.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
        bookingDTO1.setCustomerPhone(customerDTO.getPhone());
        bookingDTO1.setEmail(barberService.getBarberByBarberId(bookingDTO1.getBarberId()).getEmail());
        bookingDTO1.setBarberName(barberService.getBarberByBarberId(bookingDTO1.getBarberId()).getName());
        bookingDTO1.setBarberAvatar(barberService.getBarberByBarberId(bookingDTO1.getBarberId()).getAvatar());
        bookingDTO1.setSalonName(salonService.getSalonById(bookingDTO1.getSalonId()).getName());
        bookingDTO1.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO1.getTimeId()));
        Double price = 0.0;
        List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO1.getBookingId());
        bookingDTO1.setListBookedService(listBookedServiceDTO);
        for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
            SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
            price += salonWorkDTO.getPrice();
        }
        bookingDTO1.setPrice(price);
        BarberDTO barberDTO = barberService.getBarberByBarberId(bookingDTO1.getBarberId());
        log.info("Customer: {} book successfully! {} {}", customerDTO.getPhone(), bookingDTO1.getTimestamp(), bookingDTO1.getDate());
        // Send Noti To Manager
        try {
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String date = formatter.format(bookingDTO1.getDate());
            boolean isExist = (customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? true:false;
            String subject = "Khách hàng đặt lịch";
            String content = "Khách hàng: " + customerDTO.getPhone() + (isExist == false ? "" : "- Anh " + customerDTO.getName())+ " đã đặt lịch: " + bookingDTO1.getTimestamp()+" "+ date;
            firebaseMessagingService.sendNotiForManager(bookingDTO1, subject, content);
            // Send Noti To Barber
            firebaseMessagingService.sendNotiForBarber(bookingDTO1, subject, content);
        } catch (FirebaseMessagingException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingDTO1;
    }

    @Override
    public Object getBookedByCustomerId(Integer customerId) {

        Booking booking = bookingRepository.getBookedByCustomerId(customerId);
        if (booking == null) return "Bạn chưa đặt lịch cắt tóc";
        BookingDTO bookingDTO = Mapper.toBookingDTO(booking);
        CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
        bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
        bookingDTO.setCustomerPhone(customerDTO.getPhone());
        bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
        bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
        bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
        List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId());
        bookingDTO.setListBookedService(listBookedServiceDTO);
        Double price = 0.0;
        for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
            SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
            price += salonWorkDTO.getPrice();
        }
        bookingDTO.setPrice(price);
        return bookingDTO;
    }

    @Override
    public BookingDTO getServiceName(BookingDTO bookingDTO) {
        bookingDTO.setListBookedService(bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId()));
        return bookingDTO;
    }

    @Override
    public boolean confirmCustomerArrived(Integer bookingId, Integer barberId, JsonNode services) throws FirebaseMessagingException, ParseException {
        Optional<Booking> bookingOptional = bookingRepository.findById(bookingId);
        if (bookingOptional.isPresent()) {
            Booking booking = bookingOptional.get();
            if (booking.getBarberId() != barberId) {
                if(formatter.parse(formatter.format(Calendar.getInstance().getTime())).compareTo(timeWorkTableRepository.getTimebyId(booking.getTimeId()))<0){
                    BarberWorkTable workTable = barberWorkTableRepository.getBarberWorkTableToUpdate(booking.getBarberId(),booking.getTimeId(),booking.getDate());
                    workTable.setBookingId(null);
                    workTable.setStatus(false);
                    CustomerAssigned c = customerAssignedRepository.getByBarberId(booking.getBarberId());
                    c.setNoca(c.getNoca()+1);
                    customerAssignedRepository.save(c);
                    barberWorkTableRepository.save(workTable);
                    workTable = barberWorkTableRepository.getBarberWorkTableToUpdate(barberId,booking.getTimeId(),booking.getDate());
                    workTable.setBookingId(booking.getBookingId());
                    workTable.setStatus(true);
                    c = customerAssignedRepository.getByBarberId(barberId);
                    if(c.getNoca()>0){
                        c.setNoca(c.getNoca()-1);
                    }
                    customerAssignedRepository.save(c);
                    barberWorkTableRepository.save(workTable);
                }else{
                    BarberWorkTable workTable = barberWorkTableRepository.getBarberWorkTableToUpdate(booking.getBarberId(),booking.getTimeId(),booking.getDate());
                    workTable.setBookingId(null);
                    CustomerAssigned c = customerAssignedRepository.getByBarberId(booking.getBarberId());
                    c.setNoca(c.getNoca()+1);
                    customerAssignedRepository.save(c);
                    barberWorkTableRepository.save(workTable);
                    workTable = barberWorkTableRepository.getBarberWorkTableToUpdate(barberId,booking.getTimeId(),booking.getDate());
                    workTable.setBookingId(booking.getBookingId());
                    c = customerAssignedRepository.getByBarberId(barberId);
                    if(c.getNoca()>0){
                        c.setNoca(c.getNoca()-1);
                    }
                    customerAssignedRepository.save(c);
                    barberWorkTableRepository.save(workTable);
                }
                booking.setBarberId(barberId);
                //add change algorithm here

            }
            List<BookedServiceDTO> listBookedServiceOfBooking = bookedWorkService.getListServiceByBookingId(bookingId);
            List<Integer> listBookedServiceId = listBookedServiceOfBooking.stream().map(BookedServiceDTO::getServiceId).collect(Collectors.toList());
            List<Integer> listNewBookServiceId = new ArrayList<>();
            for (JsonNode service : services) {
                listNewBookServiceId.add(Integer.parseInt(service.asText()));
            }
            Collections.sort(listBookedServiceId);
            Collections.sort(listNewBookServiceId);
            //Case new book service : booked Service
            // [1,2,3] : [1,2]
            if (listNewBookServiceId.size() > listBookedServiceId.size() && listNewBookServiceId.containsAll(listBookedServiceId)) {
                listNewBookServiceId.removeAll(listBookedServiceId);
                for (Integer newBookedServiceId : listNewBookServiceId) {
                    bookedWorkService.addServiceToBooking(new BookedServiceDTO(booking.getBookingId(), newBookedServiceId));
                }
            }// [1,2,3] : [1, 4]
            else if (listNewBookServiceId.size() > listBookedServiceId.size() && !listNewBookServiceId.containsAll(listBookedServiceId)) {
                for (int i = 0; i < listBookedServiceId.size(); i++) {
                    if (!listNewBookServiceId.contains(listBookedServiceId.get(i))) {
                        bookedWorkService.deleteBookedServiceByBookingIdAndServiceId(booking.getBookingId(), listBookedServiceId.get(i));
                    } else {
                        listNewBookServiceId.remove(listBookedServiceId.get(i));
                    }
                }
                for (Integer newBookedServiceId : listNewBookServiceId) {
                    bookedWorkService.addServiceToBooking(new BookedServiceDTO(booking.getBookingId(), newBookedServiceId));
                }
            }// [1,2] : [1,2,3]
            else if (listNewBookServiceId.size() < listBookedServiceId.size() && listBookedServiceId.containsAll(listNewBookServiceId)) {
                listBookedServiceId.removeAll(listNewBookServiceId);
                for (Integer bookedServiceId : listBookedServiceId) {
                    bookedWorkService.deleteBookedServiceByBookingIdAndServiceId(booking.getBookingId(), bookedServiceId);
                }
            }// [1,4] : [1,2,3]
            else if (listNewBookServiceId.size() < listBookedServiceId.size() && !listBookedServiceId.containsAll(listNewBookServiceId)) {
                for (int i = 0; i < listBookedServiceId.size(); i++) {
                    if (!listNewBookServiceId.contains(listBookedServiceId.get(i))) {
                        bookedWorkService.deleteBookedServiceByBookingIdAndServiceId(booking.getBookingId(), listBookedServiceId.get(i));
                    } else {
                        listNewBookServiceId.remove(listBookedServiceId.get(i));
                    }
                }
                for (Integer newBookedServiceId : listNewBookServiceId) {
                    bookedWorkService.addServiceToBooking(new BookedServiceDTO(booking.getBookingId(), newBookedServiceId));
                }
            }// [1,2,3] : [1,3,4]
            else if (listBookedServiceId.size() == listNewBookServiceId.size() && !(listBookedServiceId.equals(listNewBookServiceId))) {
                for (int i = 0; i < listNewBookServiceId.size(); i++) {
                    if (!listNewBookServiceId.contains(listBookedServiceId.get(i))) {
                        bookedWorkService.deleteBookedServiceByBookingIdAndServiceId(booking.getBookingId(), listBookedServiceId.get(i));
                    } else {
                        listNewBookServiceId.remove(listBookedServiceId.get(i));
                    }
                }
                for (Integer newBookedServiceId : listNewBookServiceId) {
                    bookedWorkService.addServiceToBooking(new BookedServiceDTO(booking.getBookingId(), newBookedServiceId));
                }
            }
            // [1,2,3] : [1,2,3]
            booking.setStatus(BookingStatus.ARRIVED);
            Booking bookingResult = bookingRepository.save(booking);
            if (bookingResult != null) {
                CustomerDTO customerDTO = customerService.getCustomerDetailById(booking.getCustomerId());
                BookingDTO bookingDTO = Mapper.toBookingDTO(bookingResult);
                // send noti to Barber
                try{
                    boolean isExist = (customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? true:false;
                String subject = "Khách hàng đã đến";
                String content = "Khách hàng: " + customerDTO.getPhone() + (isExist == false ? "" : "- Anh " + customerDTO.getName()) + " đã đến. Vui lòng chuẩn bị cắt cho khách.";
                firebaseMessagingService.sendNotiForBarber(bookingDTO, subject, content);
                }catch(FirebaseMessagingException ex){
                    ex.printStackTrace();
                }catch (Exception e){
                    e.printStackTrace();
                }
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public BookingDTO confirmComplete(Integer bookingId) throws ParseException {
        Optional<Booking> bookingOptional = bookingRepository.getBookingByBookingId(bookingId);
        if (bookingOptional.isPresent()) {
            Booking booking = bookingOptional.get();
            booking.setStatus(BookingStatus.COMPLETED);
            Double price = 0.0;
            Booking bookingResult = bookingRepository.save(booking);
            if (bookingResult != null) {
                BookingDTO bookingDTO = Mapper.toBookingDTO(bookingResult);
                CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
                bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
                bookingDTO.setCustomerPhone(customerDTO.getPhone());
                bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
                bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
                bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
                List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId());
                bookingDTO.setListBookedService(listBookedServiceDTO);
                for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
                    SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                    price += salonWorkDTO.getPrice();
                }
                bookingDTO.setPrice(price);
                boolean isSaveHisSuccess = historyService.createHistory(bookingDTO);
                try {
                    String subject = "Đánh giá";
                    String content = "Bạn có cảm thấy hài lòng với lần cắt vừa xong ?";
                    firebaseMessagingService.sendNotiForCustomer(bookingDTO, subject, content);
                } catch (FirebaseMessagingException ex) {
                    ex.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return bookingDTO;
            }
        }
        return null;
    }

    @Override
    public boolean confirmServingCustomer(Integer bookingId) {
        Optional<Booking> bookingOptional = bookingRepository.getBookingByBookingId(bookingId);
        if (bookingOptional.isPresent()) {
            Booking booking = bookingOptional.get();
            booking.setStatus(BookingStatus.SERVING);
            bookingRepository.save(booking);
            return true;
        }
        return false;
    }


    @Override
    public Object managerBooking(BookingDTO bookingDTO, Customer customer) throws ParseException, FirebaseMessagingException {
        BarberDTO barber = barberService.getBarberByPhone(customer.getPhone());

        if(barber!=null) return "Barber không thể tự cắt cho bản thân ";
        Customer result = customerRepository.getCustomerByPhone(customer.getPhone());
        if (result == null) {
            customer.setRoleId(1);
            customer.setRank_id(1);
            customer.setTotal_purchase(0.0);
            bookingDTO.setCustomerId(customerRepository.save(customer).getId());
        } else {
            bookingDTO.setCustomerId(result.getId());
        }
        return insertNewBooking(bookingDTO, 3);
    }

    @Override
    public Object managerBookingExistedCustomer(BookingDTO bookingDTO) throws ParseException, FirebaseMessagingException {
        CustomerDTO customer = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
        BarberDTO barber = barberService.getBarberByPhone(customer.getPhone());
        if(barber!=null) return "barber không thể tự cắt cho bản thân ";
        return insertNewBooking(bookingDTO, 3);
    }

    @Override
    public List<BookingDTO> getAllListBillingOfSalon(Integer salonId) {
        SalonDTO salonDTO = salonService.getSalonById(salonId);
        if (salonDTO != null) {
            List<BookingDTO> listBookingDTO = new ArrayList<>();
            List<Booking> listBooking = bookingRepository.getListBillingBySalonId(salonId);
            if (listBooking != null && !listBooking.isEmpty()) {
                for (Booking booking : listBooking) {
                    BookingDTO bookingDTO = Mapper.toBookingDTO(booking);
                    CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
                    bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
                    bookingDTO.setCustomerPhone(customerDTO.getPhone());
                    bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
                    bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
                    bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
                    bookingDTO.setListBookedService(bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId()));
                    Double price = 0.0;
                    for (BookedServiceDTO bookedServiceDTO : bookingDTO.getListBookedService()) {
                        price += salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId()).getPrice();
                    }
                    bookingDTO.setPrice(price);
                    listBookingDTO.add(bookingDTO);
                }
                return listBookingDTO;
            }
            return listBookingDTO;
        }
        return null;
    }

    @Override
    public List<BookingDTO> getListWorkOfDayForBarber(Integer barberId, int page, String status) {
        List<Booking> bookings;
        if (status.equals("")) {
            bookings = bookingRepository.getListWorkOfDayForBarber(barberId, (page - 1) * 5, "%%");
        } else {
            bookings = bookingRepository.getListWorkOfDayForBarber(barberId, (page - 1) * 5, "%" + status + "%");
        }
        if (bookings.isEmpty()) {
            return null;
        }
        List<BookingDTO> bookingDTOS = new ArrayList<>();
        for (Booking booking : bookings) {
            BookingDTO bookingDTO = Mapper.toBookingDTOWithoutImage(booking);
            CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
            bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
            bookingDTO.setCustomerPhone(customerDTO.getPhone());
            bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
            bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
            bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
            Double price = 0.0;
            List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId());
            bookingDTO.setListBookedService(listBookedServiceDTO);
            for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
                SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                price += salonWorkDTO.getPrice();
            }
            bookingDTO.setPrice(price);
            bookingDTOS.add(bookingDTO);
        }
        return bookingDTOS;
    }

    @Override
    public Object getAllListBookingBySalonIdAndStatus(Integer salonId, String status, int page) {
        SalonDTO salonDTO = salonService.getSalonById(salonId);
        if (salonDTO != null) {
            List<BookingDTO> listBookingDTO = new ArrayList<>();
            List<Booking> listBooking = (status == null || status.equals("")) ? bookingRepository.findBookingsBySalonId(salonId, (page - 1) * 5) : bookingRepository.findBookingsBySalonIdAndStatus(salonId, status, (page - 1) * 5);
            if (listBooking != null) {
                for (Booking booking : listBooking) {
                    BookingDTO bookingDTO = Mapper.toBookingDTO(booking);
                    CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
                    bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
                    bookingDTO.setCustomerPhone(customerDTO.getPhone());
                    bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
                    bookingDTO.setBarberAvatar(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getAvatar());
                    bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
                    bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
                    List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId());
                    bookingDTO.setListBookedService(listBookedServiceDTO);
                    Double price = 0.0;
                    for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
                        SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                        price += salonWorkDTO.getPrice();
                    }
                    bookingDTO.setPrice(price);
                    listBookingDTO.add(bookingDTO);
                }
            }
            return listBookingDTO;
        }
        return null;
    }

    @Override
    public String customerRating(Integer bookingId, Integer rate) {
        Optional<Booking> bookingOptional = bookingRepository.getBookingByBookingId(bookingId);
        if (bookingOptional.isPresent()) {
            Booking booking = bookingOptional.get();
            booking.setStar(rate);
            String updateBarberRank = barberService.updateBarberPointByRating(booking.getBarberId(), booking.getCustomerId(), rate);
            boolean isSuccess = historyService.updateStarToHistory(bookingId, rate);
        } else return null;
        return "Cảm ơn bạn đã đánh giá";
    }

    private final Random rand = new Random();

    public Integer bookRandom(Integer salonId, Integer timeId, Date date, int role) {
        List<Integer> listBarberFree;
        if (role == 3) {
            listBarberFree = barberWorkTableRepository.getFreeBarberIdForManager(salonId, timeId, date);
        } else listBarberFree = barberWorkTableRepository.getFreeBarberId(salonId, timeId, date);
        List<Integer> random = new ArrayList<>();

        if (listBarberFree.size() == 0) return 0;
        else {
            for (Integer barber : listBarberFree) {
                int no = customerAssignedRepository.getNumberAssigned(barber);
                if (no > 0) {
                    for (int i = 0; i < no; i++) {
                        random.add(barber);
                    }
                }
            }
            if (random.isEmpty()) {
                random.addAll(listBarberFree);
            }
            return random.get(rand.nextInt(random.size()));
        }
    }

    @Override
    public BookingDTO getBookingDTODetailByBookingId(Integer bookingId) {
        Optional<Booking> bookingOptional = bookingRepository.getBookingByBookingId(bookingId);
        if (bookingOptional.isPresent()) {
            Booking booking = bookingOptional.get();
            return Mapper.toBookingDTO(booking);
        }
        return null;
    }

    @Override
    public Object getAllListBookingByDateAndPhone(Integer salonId, String phone) throws ParseException {
        List<BookingDTO> listBookingDTO = new ArrayList<>();
        if (phone == "" || phone == null) {
            List<Booking> listBooking = bookingRepository.getListBookingBySalonIdAndStatusWait(salonId);
            for (Booking booking : listBooking) {
                BookingDTO bookingDTO = Mapper.toBookingDTO(booking);
                CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
                bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
                bookingDTO.setCustomerPhone(customerDTO.getPhone());
                bookingDTO.setEmail(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getEmail());
                bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
                bookingDTO.setBarberAvatar(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getAvatar());
                bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
                bookingDTO.setListBookedService(bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId()));
                bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
                Double price = 0.0;
                List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId());
                bookingDTO.setListBookedService(listBookedServiceDTO);
                for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
                    SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                    price += salonWorkDTO.getPrice();
                }
                bookingDTO.setPrice(price);
                listBookingDTO.add(bookingDTO);
            }
            return listBookingDTO;
        } else if (customerRepository.getCustomerByPhone(phone) == null) {
            return "Số điện thoại không tồn tại";
        } else {
            CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(phone);
            if (customerDTO == null) return "Bạn chưa có lịch đặt. Vui lòng đặt lịch";
            Optional<Booking> bookingOptional = bookingRepository.getBookingBySalonIdAndDateAndCustomerId(customerDTO.getId(), salonId);
            if (bookingOptional.isPresent()) {
                Booking booking = bookingOptional.get();
                BookingDTO bookingDTO = Mapper.toBookingDTO(booking);
                bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
                bookingDTO.setEmail(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getEmail());
                bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
                bookingDTO.setBarberAvatar(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getAvatar());
                bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
                bookingDTO.setListBookedService(bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId()));
                bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
                Double price = 0.0;
                List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId());
                bookingDTO.setListBookedService(listBookedServiceDTO);
                for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
                    SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                    price += salonWorkDTO.getPrice();
                }
                bookingDTO.setPrice(price);
                listBookingDTO.add(bookingDTO);
            }
            return listBookingDTO;
        }
    }

    @Override
    public BookingDTO insertImagesToBooking(Integer bookingId, String frontPic, String leftPic, String behindPic, String rightPic) throws IOException, ParseException {
        Optional<Booking> bookingOptional = bookingRepository.findById(bookingId);
        if (bookingOptional.isPresent()) {
            Booking booking = bookingOptional.get();

            booking.setFrontPic(frontPic);
            booking.setLeftPic(leftPic);
            booking.setBehindPic(behindPic);
            booking.setRightPic(rightPic);
            booking.setStatus(BookingStatus.BILLING);
            Booking bookingResult = bookingRepository.save(booking);
            BookingDTO bookingDTO = Mapper.toBookingDTO(bookingResult);
            CustomerDTO customerDTO = customerService.getCustomerDetailById(bookingDTO.getCustomerId());
            bookingDTO.setCustomerName((customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? customerDTO.getName() :"");
            bookingDTO.setCustomerPhone(customerDTO.getPhone());
            bookingDTO.setBarberName(barberService.getBarberByBarberId(bookingDTO.getBarberId()).getName());
            bookingDTO.setSalonName(salonService.getSalonById(bookingDTO.getSalonId()).getName());
            bookingDTO.setTimestamp(timeWorkTableRepository.getTimebyId(bookingDTO.getTimeId()));
            Double price = 0.0;
            List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(bookingDTO.getBookingId());
            bookingDTO.setListBookedService(listBookedServiceDTO);
            for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
                SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                price += salonWorkDTO.getPrice();
            }
            bookingDTO.setPrice(price);
            //Update barber point by service
            List<Integer> listBookedService = new ArrayList<>();
            for (BookedServiceDTO bookedService : bookingDTO.getListBookedService()) {
                listBookedService.add(bookedService.getServiceId());
            }
            String updateBarberPointResult = barberService.updateBarberPointByService(booking.getBarberId(), booking.getCustomerId(), listBookedService);
            log.info(updateBarberPointResult);
            try {
                boolean isExist = (customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? true:false;
                String subject = "Khách hàng đã sử dụng dịch vụ xong";
                String content = "Khách hàng: " + customerDTO.getPhone() + (isExist == false ? "" : "- Anh " + customerDTO.getName()) + " đã cắt xong. Chuẩn bị thanh toán.";
                firebaseMessagingService.sendNotiForManager(bookingDTO, subject, content);
            } catch (FirebaseMessagingException ex) {
                ex.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //Get token of Manager
            return bookingDTO;
        }
        return null;
    }

    @Override
    public String cancelBooking(Integer bookingId) throws ParseException, FirebaseMessagingException {
        Booking booking = bookingRepository.getBookedByBookingId(bookingId);
        if (booking == null) return "";
        cancelBook(booking.getSalonId(), booking.getTimeId(), booking.getDate(), booking.getBarberId());
        CustomerAssigned customerAssigned = customerAssignedRepository.getByBarberId(booking.getBarberId());
        customerAssigned.setNoca(customerAssigned.getNoca() + 1);
        customerAssignedRepository.save(customerAssigned);
        booking.setStatus(BookingStatus.CANCELLED);
        Booking bookingResult = bookingRepository.save(booking);
        CustomerDTO customerDTO = customerService.getCustomerDetailById(booking.getCustomerId());
        ManagerDTO managerDTO = managerService.getManagerDetailBySalonId(booking.getSalonId());
        BarberDTO barberDTO = barberService.getBarberByBarberId(booking.getBarberId());
        BookingDTO bookingDTO = Mapper.toBookingDTO(bookingResult);
        try {
            boolean isExist = (customerDTO.getName() != null && !customerDTO.getName().isEmpty()) == true ? true:false;
            String subject = "Khách hàng hủy lịch";
            String content = "Khách hàng: " + customerDTO.getPhone() + (isExist == false ? "" : "- Anh " + customerDTO.getName()) + " đã hủy đặt lịch";
            //send noti for manager
            firebaseMessagingService.sendNotiForManager(bookingDTO, subject, content);
            //Send noti for barber
            firebaseMessagingService.sendNotiForBarber(bookingDTO, subject, content);
        } catch (FirebaseMessagingException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Huỷ lịch thành công";
    }


    private void cancelBook(Integer salonId, Integer timeId, Date date, Integer barberId) throws ParseException {
        SalonWorkTable salonWorkTable = salonWorkTableRepository.getSalonWorkTableToUpdate(salonId, timeId, date);
        BarberWorkTable barberWorkTable = barberWorkTableRepository.getBarberWorkTableToUpdate(barberId, timeId, date);
        if (salonWorkTable == null || barberWorkTable == null) return;
        Date now = formatter.parse(formatter.format(java.util.Calendar.getInstance().getTime()));
        Date time = timeWorkTableRepository.getTimebyId(timeId);
        Date day = formatter.parse(formatter.format(java.util.Calendar.getInstance().getTime()));
        if ((day.compareTo(date)<0)|| (now.compareTo(time) < 0 && day.compareTo(date)==0)) {
            salonWorkTable.setSlot(salonWorkTable.getSlot() + 1);
            barberWorkTable.setStatus(true);
        }
        barberWorkTable.setBookingId(null);
        salonWorkTableRepository.save(salonWorkTable);
        barberWorkTableRepository.save(barberWorkTable);
    }


    private Integer timeId = 1;

    @Scheduled(cron = "0 0/15 8-19 * * ?")
    public void autoCancel() throws ParseException {
        if (timeId == 21) {
            timeId = 1;
            return;
        }
        long time = timeWorkTableRepository.getTimebyId(timeId).getTime();
        long now = formatter.parse(formatter.format(java.util.Calendar.getInstance().getTime())).getTime();
        if (now - time == 900000) {
            List<Booking> bookings = bookingRepository.getBookingToAutoCancel(timeId, java.util.Calendar.getInstance().getTime());
            cancelAuto(bookings);
            timeId++;
        }
    }

    private void cancelAuto(List<Booking> bookings) throws ParseException {
        if (!bookings.isEmpty()) {
            for (Booking booking : bookings) {
                cancelBook(booking.getSalonId(), booking.getTimeId(), booking.getDate(), booking.getBarberId());
                CustomerAssigned customerAssigned = customerAssignedRepository.getByBarberId(booking.getBarberId());
                customerAssigned.setNoca(customerAssigned.getNoca() + 1);
                customerAssignedRepository.save(customerAssigned);
                booking.setStatus(BookingStatus.CANCELLED);
                bookingRepository.save(booking);
            }
        }
    }

    @Override
    public void cancelPastBooking() {
        try {
            List<Booking> preBookings = bookingRepository.getBookingsByDateBeforeAndStatus(java.util.Calendar.getInstance().getTime(), BookingStatus.WAIT);
            cancelAuto(preBookings);
            long date = formatter.parse(formatter.format(java.util.Calendar.getInstance().getTime())).getTime();
            if (date > formatter.parse("18:45:00").getTime()) {
                for (int i = 1; i <= 20; i++) {
                    List<Booking> bookings = bookingRepository.getBookingToAutoCancel(i, java.util.Calendar.getInstance().getTime());
                    cancelAuto(bookings);
                }
                timeId = 1;
                return;
            }
            if (date < formatter.parse("08:15:00").getTime()) return;
            List<TimeWorkTable> timeWorkTableList = timeWorkTableRepository.getAllTimeWorkTable();
            for (TimeWorkTable t : timeWorkTableList) {
                long time = t.getTimeStamp().getTime();
                if (date < time) {
                    if (date - timeWorkTableRepository.getTimebyId(t.getTimeId() - 1).getTime() < 900000) {
                        for (int i = 1; i <= t.getTimeId() - 2; i++) {
                            List<Booking> bookings = bookingRepository.getBookingToAutoCancel(i, java.util.Calendar.getInstance().getTime());
                            cancelAuto(bookings);
                        }
                        timeId = t.getTimeId() - 1;
                    } else {
                        for (int i = 1; i <= t.getTimeId() - 1; i++) {
                            List<Booking> bookings = bookingRepository.getBookingToAutoCancel(i, java.util.Calendar.getInstance().getTime());
                            cancelAuto(bookings);
                        }
                        timeId = t.getTimeId();
                    }
                    break;
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
