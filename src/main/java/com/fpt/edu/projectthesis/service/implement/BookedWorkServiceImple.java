package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.BookedService;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.BookedServiceDTO;
import com.fpt.edu.projectthesis.model.SalonWorkDTO;
import com.fpt.edu.projectthesis.repository.BookedServiceRepository;
import com.fpt.edu.projectthesis.service.BookedWorkService;
import com.fpt.edu.projectthesis.service.SalonWorkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class BookedWorkServiceImple implements BookedWorkService {

    @Autowired
    private BookedServiceRepository bookedServiceRepository;

    @Autowired
    private SalonWorkService salonWorkService;

    @Override
    public void addServiceToBooking(BookedServiceDTO bookedServiceDTO) {
        bookedServiceRepository.save(Mapper.toBookedService(bookedServiceDTO));
    }

    @Override
    public List<BookedServiceDTO> getListServiceByBookingId(Integer bookingId) {
        Optional<List<BookedService>> listBookedServiceOptional = bookedServiceRepository.getAllByBookingId(bookingId);
        List<BookedService> listBookedService = listBookedServiceOptional.orElse(null);
        if(listBookedService == null || listBookedService.isEmpty()){
            return null;
        }else{
            List<BookedServiceDTO> listBookedServiceDTO = new ArrayList<>();
            for(BookedService bookedService : listBookedService){
                BookedServiceDTO bookedServiceDTO = Mapper.toBookedServiceDTO(bookedService);
                SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                bookedServiceDTO.setService_name(salonWorkDTO.getName());
                listBookedServiceDTO.add(bookedServiceDTO);
            }
            return listBookedServiceDTO;
        }
    }

    @Override
    public void deleteListServiceByBookingId(Integer bookingId) {
        List<BookedService> listBookedService = bookedServiceRepository.findBookedServiceByBookingId(bookingId);
        if(listBookedService!=null && !listBookedService.isEmpty()){
        bookedServiceRepository.deleteBookedServiceByBookingId(bookingId);
        }
    }

    @Override
    public void deleteBookedServiceByBookingIdAndServiceId(Integer bookingId, Integer bookedServiceId) {
        bookedServiceRepository.deleteBookedServiceByBookingIdAndBookedServiceId(bookingId,bookedServiceId);
    }


}
