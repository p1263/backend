package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.BookingDTO;
import com.fpt.edu.projectthesis.model.HistoryDTO;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public interface HistoryService {

    List<HistoryDTO> getListHistoryByCustomerId(Integer customer_id);

    HistoryDTO getHistoryById(Integer id);

    boolean createHistory(BookingDTO bookingDTO) throws ParseException;

    HistoryDTO getLatestHistoryOfCustomer(Integer customerId);

    boolean updateStarToHistory(Integer bookingId, Integer star);
}
