package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.Account;
import com.fpt.edu.projectthesis.entity.Customer;
import com.fpt.edu.projectthesis.entity.Role;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.AccountDTO;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import com.fpt.edu.projectthesis.repository.AccountRepository;
import com.fpt.edu.projectthesis.repository.CustomerRepository;
import com.fpt.edu.projectthesis.repository.RoleRepository;
import com.fpt.edu.projectthesis.service.AccountService;
import com.fpt.edu.projectthesis.service.CustomerService;
import com.fpt.edu.projectthesis.service.ValidateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class AccountServiceImple implements AccountService, UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ValidateService validateService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public boolean createAccount(AccountDTO accountDTO) throws ParseException {
        // check sdt exist
        if (!isAccountExist(accountDTO.getPhone())) {
            Account account = new Account();
            account.setPhone(accountDTO.getPhone());
            account.setPassword(passwordEncoder.encode(accountDTO.getPassword()));
            account.setRoleId(1);
            Account accountSave = accountRepository.save(account);
            if (accountSave != null) {
                CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(accountSave.getPhone());
                if (customerDTO == null) {
                    Customer customer = new Customer();
                    customer.setPhone(accountSave.getPhone());
                    customer.setRoleId(account.getRoleId());
                    customer.setRank_id(1);
                    customer.setTotal_purchase(0.0);
                    customerRepository.save(customer);
                    return true;
                }
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public AccountDTO getAccountDetailsByPhone(String phone) {
        Optional<Account> account = accountRepository.findAccountByPhone(phone);
        if (account.isPresent()) {
            AccountDTO accountDTO = Mapper.toAccountDTO(account.get());
            accountDTO.setPassword("");
            return accountDTO;
        } else return null;
    }


    @Override
    public boolean isAccountExist(String phone) {
        Optional<Account> account = accountRepository.findAccountByPhone(phone);
        if (account.isPresent()) {
            return true;
        } else return false;
    }

    @Override
    public boolean changePassword(String password, String phone) {
        Integer result = accountRepository.changePassword(passwordEncoder.encode(password), phone);
        if (result != 0) return true;
        return false;
    }

    @Override
    public String changeAccountRole(String phone, int role) {

        Integer result = accountRepository.changeRoleAccount(role, phone);
        if (result > 0) {
            log.info("Change account role of Account: {} Successfully", phone);
            return "SUCCESS";
        }
        return "FAIL";
    }

    @Override
    public Integer getRoleOfAccountByPhone(String phone) {
        Integer roleId = accountRepository.getRoleIdOfAccountByPhone(phone);
        return roleId;
    }

    @Override
    public void insertDeviceTokenToAccount(String phone, String token) {
        Optional<Account> accountOptional = accountRepository.findAccountByPhone(phone);
        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            account.setDeviceToken(token);
            accountRepository.save(account);
            log.info("Phone: {} - device token: {}", phone, token);
        } else {
            log.info("Phone: {} not exist! Cannot insert device token", phone);
        }
    }

    @Override
    public String getAccountToken(String phone) {
        String deviceToken = accountRepository.getDeviceTokenByPhone(phone);
        return deviceToken;
    }

    @Override
    public String changeNewPassword(Integer accountId, String oldPwd, String newPwd) {
        Optional<Account> accountOptional = accountRepository.findById(accountId);
        if(accountOptional.isPresent()){
            Account account = accountOptional.get();
            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
            if(bCryptPasswordEncoder.matches(oldPwd,account.getPassword())){
                if(!newPwd.equals(oldPwd)){
                    account.setPassword(passwordEncoder.encode(newPwd));
                    accountRepository.save(account);
                    return "SUCCESS";
                }return "Mật khẩu mới không được trùng với mật khẩu cũ";
            }return "Mật khẩu cũ không đúng";
        }return "Account không tồn tại";
    }

    @Override
    public boolean logOut(String phone) {
        Optional<Account> accountOptional = accountRepository.findAccountByPhone(phone);
        if(accountOptional.isPresent()){
            Integer result = accountRepository.removeDeviceTokenFromAccount(phone);
            if(result>0){
                return true;
            }
            return false;
        }
        return false;
    }

    @Override
    public UserDetails loadUserByUsername(String phone) throws UsernameNotFoundException {
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        try {
            Optional<Account> accountOptional = accountRepository.findAccountByPhone(phone);
            if (!accountOptional.isPresent()) {
                throw new UsernameNotFoundException("User not found in db");
            } else {
                log.info("User found in database: {}", phone);
                Role role = roleRepository.getRoleById(accountOptional.get().getRoleId());
                authorities.add(new SimpleGrantedAuthority(role.getName()));
                return new User(accountOptional.get().getPhone(), accountOptional.get().getPassword(), authorities);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;

    }
}
