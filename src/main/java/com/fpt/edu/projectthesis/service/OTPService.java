package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.entity.OTPTemp;

import java.io.IOException;

public interface OTPService {

    boolean createOTP(String phone);

    String sendGetJSON(String phone) throws IOException;

    String resendOTPJSON(String phone) throws IOException;

    boolean checkEqualsOTP(String phone, String OTPCode);
}
