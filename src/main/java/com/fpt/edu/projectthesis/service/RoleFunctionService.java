package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.entity.RoleFunction;

import java.util.List;

public interface RoleFunctionService {

    List<RoleFunction> getAllFunctionOfRole(Integer roleId);
}
