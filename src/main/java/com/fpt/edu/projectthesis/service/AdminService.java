package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.AdminDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

public interface AdminService {

    AdminDTO createAdmin(AdminDTO adminDTO) throws ParseException;

    AdminDTO getInfoAdminByPhone(String phone) throws ParseException;

    AdminDTO updateInfoAdmin(AdminDTO adminDTO) throws ParseException;

    AdminDTO insertAvatarToAdmin(String phone, MultipartFile avatar) throws IOException, ParseException;

    AdminDTO getInfoAdminById(Integer id);

}
