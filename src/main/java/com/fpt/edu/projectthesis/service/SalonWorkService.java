package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.entity.SalonWork;
import com.fpt.edu.projectthesis.model.SalonWorkDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface SalonWorkService {

    List<SalonWorkDTO> getAllSalonWork();

    SalonWorkDTO getSalonWorkById(Integer id);

    SalonWorkDTO createSalonService(SalonWorkDTO salonWorkDTO);

    SalonWorkDTO updateSalonService(SalonWorkDTO salonWorkDTO);

    boolean deleteSalonService(Integer salonServiceId);
}
