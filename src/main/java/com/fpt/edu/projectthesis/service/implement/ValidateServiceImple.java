package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.service.ValidateService;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class ValidateServiceImple implements ValidateService {


    @Override
    public boolean isValidPhoneAndNotNull(String phone) {
        if(phone == null) return false;
        Pattern pattern = Pattern.compile("^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$");
        Matcher matcher = pattern.matcher(phone);
        return (matcher.matches());
    }

    @Override
    public boolean isValidEmail(String email) {
        if(email == null) return false;
        Pattern pattern = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$");
        Matcher matcher = pattern.matcher(email);
        return (matcher.matches());
    }

    @Override
    public boolean isValidPassword(String password) {
        if(password == null) return false;
        Pattern pattern = Pattern.compile("(^.{8,32})");
        Matcher matcher = pattern.matcher(password);
        return (matcher.matches());
    }

    @Override
    public boolean isValidNumber(String inputString) {
        if(inputString == null || inputString == "") return false;
        Pattern pattern = Pattern.compile("^\\d+$");
        Matcher matcher = pattern.matcher(inputString);
        return (matcher.matches());
    }

    @Override
    public boolean isValidPhoneAllowNull(String phone) {
        if(phone == null || phone == "") return true;
        Pattern pattern = Pattern.compile("^(0?)(3[2-9]|5[6|8|9]|7[0|6-9]|8[0-6|8|9]|9[0-4|6-9])[0-9]{7}$");
        Matcher matcher = pattern.matcher(phone);
        return (matcher.matches());
    }

    @Override
    public boolean isValidDate(String date) {
        if(date == null) return false;
        Pattern pattern = Pattern.compile("^\\d{4}-\\d{2}-\\d{2}$");
        Matcher matcher = pattern.matcher(date);
        return (matcher.matches());
    }

}
