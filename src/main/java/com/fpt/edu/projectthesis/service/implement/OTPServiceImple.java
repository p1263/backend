package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.OTPTemp;
import com.fpt.edu.projectthesis.repository.OTPTempRepository;
import com.fpt.edu.projectthesis.service.OTPService;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class OTPServiceImple implements OTPService {

    final String APIKey = "E79AEBFDEDD8F605C4D7A160B13852";
    final String SecretKey = "A43BC135817FCAC02BE051EDCC64CD";
    final String messageContent = " la ma xac minh dang ky Baotrixemay cua ban";

    @Autowired
    private OTPTempRepository otpTempRepository;

    @Override
    public boolean createOTP(String phone) {
        try {
            Optional<OTPTemp> otpTempExist = otpTempRepository.findOTPTempByPhone(phone);
            String otp = String.valueOf((int) Math.floor(((Math.random() * 899999) + 100000)));

            if (otpTempExist.isPresent()) {
                OTPTemp otpTemp = otpTempExist.get();
                otpTemp.setOtpcode(otp);
                Date createdDate = new Date(System.currentTimeMillis());
                otpTemp.setCreatedDate(createdDate);
                otpTemp.setResendDate(null);
                otpTempRepository.save(otpTemp);
                return true;
            } else {
                OTPTemp otpTemp = new OTPTemp();
                otpTemp.setPhone(phone);
                otpTemp.setOtpcode(otp);
                Date createdDate = new Date(System.currentTimeMillis());
                otpTemp.setCreatedDate(createdDate);
                otpTempRepository.save(otpTemp);
                return true;
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return false;
        }
    }

    @Override
    public String sendGetJSON(String phone) throws IOException {
        boolean isCreateOTP = createOTP(phone);
        String serviceMessage = "";
        if (isCreateOTP == false) return serviceMessage = "create OTP fail";
        Optional<OTPTemp> otpTempOptional = otpTempRepository.findOTPTempByPhone(phone);
        String message = otpTempOptional.get().getOtpcode() + messageContent;
        String url = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?ApiKey=" + URLEncoder.encode(APIKey, "UTF-8")
                + "&secretKey=" + URLEncoder.encode(SecretKey, "UTF-8")
                + "&SmsType=2&Phone=" + URLEncoder.encode(phone, "UTF-8")
                + "&Content=" + URLEncoder.encode(message, "UTF-8")
                + "&Brandname=Baotrixemay";
        URL obj;
        try {
            obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");
            int responseCode = con.getResponseCode();
            log.info("Sending 'GET' request to SMS API - phone: {} - otp {}", phone, otpTempOptional.get().getOtpcode());
            log.info("Response Code: {}", responseCode);
            if (responseCode == 200) {
                // Check CodeResult
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONObject json = (JSONObject) new JSONParser().parse(response.toString());
                int CodeReSult = Integer.parseInt(String.valueOf(json.get("CodeResult")));
                if (CodeReSult == 100) {
                    log.info("Send OTP messsage to {} successfully", phone);
                    log.info("SMSID = {}", json.get("SMSID"));
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    return serviceMessage = "SUCCESS";
                } else if (CodeReSult == 99) {
                    log.info("Send OTP messsage to {} fail - error: Unknown", phone);
                    log.info("SMSID = {}", json.get("SMSID"));
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    return serviceMessage = "Fail - Unknown Error";
                } else if (CodeReSult == 101) {
                    log.info("SMSID = {}", json.get("SMSID"));
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    log.info("SMSID = {}", json.get("SMSID"));
                    return serviceMessage = "Fail - Wrong Key";
                } else if (CodeReSult == 103) {
                    log.info("Send OTP messsage to {} fail - Not enough money", phone);
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    log.info("SMSID = {}", json.get("SMSID"));
                    return serviceMessage = "Fail - Not enough money";
                } else return "Fail - Unknown Code Error";
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "FAIL - ERROR WHEN CALL API SEND MESSAGE";
    }

    @Override
    public String resendOTPJSON(String phone) throws IOException {
        String serviceMessage = "";
        String otp = "";
        Optional<OTPTemp> otpTempOptional = otpTempRepository.findOTPTempByPhone(phone);
        OTPTemp otpTemp = otpTempOptional.orElse(null);
        if (otpTemp == null) return serviceMessage = "Resend fail! Phone number not exist";
        Date resendDate = otpTemp.getResendDate();
        if (resendDate == null) {
            try {
                otp = String.valueOf((int) Math.floor(((Math.random() * 899999) + 100000)));
                otpTemp.setOtpcode(otp);
                otpTemp.setResendDate(new Date(System.currentTimeMillis()));
                otpTemp.setCreatedDate(new Date(System.currentTimeMillis()));
                otpTempRepository.save(otpTemp);
                log.info("Generate new OTP for phone number {} successfully", phone);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        } else {
            long timeDistance = System.currentTimeMillis() - resendDate.getTime();
            if (timeDistance > (2 * 60 * 1000)) {
                try {
                    otp = String.valueOf((int) Math.floor(((Math.random() * 899999) + 100000)));
                    otpTemp.setOtpcode(otp);
                    otpTemp.setResendDate(new Date(System.currentTimeMillis()));
                    otpTemp.setCreatedDate(new Date(System.currentTimeMillis()));
                    otpTempRepository.save(otpTemp);
                    log.info("Generate new OTP for phone number {} successfully", phone);
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            } else {
                return serviceMessage = "You must wait 2 minutes to call resend OTP api";
            }
        }
        String message = otpTempOptional.get().getOtpcode() + messageContent;
        String url = "http://rest.esms.vn/MainService.svc/json/SendMultipleMessage_V4_get?ApiKey=" + URLEncoder.encode(APIKey, "UTF-8")
                + "&secretKey=" + URLEncoder.encode(SecretKey, "UTF-8")
                + "&SmsType=2&Phone=" + URLEncoder.encode(phone, "UTF-8")
                + "&Content=" + URLEncoder.encode(message, "UTF-8")
                + "&Brandname=Baotrixemay";
        URL obj;
        try {
            obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");
            int responseCode = con.getResponseCode();
            log.info("Sending 'GET' request to SMS API - phone: {} - otp {}", phone, otpTemp.getOtpcode());
            log.info("Response Code: {}", responseCode);
            if (responseCode == 200) {
                // Check CodeResult
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONObject json = (JSONObject) new JSONParser().parse(response.toString());
                int CodeReSult = Integer.parseInt(String.valueOf(json.get("CodeResult")));
                if (CodeReSult == 100) {
                    log.info("Send OTP messsage to {} successfully", phone);
                    log.info("SMSID = {}", json.get("SMSID"));
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    return message = "SUCCESS";
                } else if (CodeReSult == 99) {
                    log.info("Send OTP messsage to {} fail - error: Unknown", phone);
                    log.info("SMSID = {}", json.get("SMSID"));
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    return message = "Fail - Unknown Error";
                } else if (CodeReSult == 101) {
                    log.info("SMSID = {}", json.get("SMSID"));
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    log.info("SMSID = {}", json.get("SMSID"));
                    return message = "Fail - Wrong Key";
                } else if (CodeReSult == 103) {
                    log.info("Send OTP messsage to {} fail - Not enough money", phone);
                    log.info("ErrorMessage = {}", json.get("ErrorMessage"));
                    log.info("SMSID = {}", json.get("SMSID"));
                    return message = "Fail - Not enough money";
                } else return "Fail - Unknown Code Error";
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return "FAIL - ERROR WHEN CALL API SEND MESSAGE";

    }

    @Override
    public boolean checkEqualsOTP(String phone, String OTPCode) {
        Optional<OTPTemp> otpTempOptional = otpTempRepository.findOTPTempByPhone(phone);
        OTPTemp otpTemp = otpTempOptional.orElse(null);
        if (otpTemp == null) {
            return false;
        } else {
            boolean checkPhone = otpTemp.getPhone().equals(phone);
            boolean checkOTP = otpTemp.getOtpcode().equals(OTPCode);
            if (checkPhone == true && checkOTP == true) {
                return true;
            } else return false;
        }
    }

    @Scheduled(cron = "0 0/30 * * * ?")
    public void deleteOTP() {
        List<OTPTemp> listOTP = otpTempRepository.findOTPTempsNotNullAndNotBlank();
        if (!listOTP.isEmpty() && listOTP != null) {
            for (OTPTemp otpTemp : listOTP) {
                Date createdDate = otpTemp.getCreatedDate();
                long timeDistance = System.currentTimeMillis() - createdDate.getTime();
                if (timeDistance > 30 * 60 * 1000) {
                    otpTemp.setOtpcode(null);
                    otpTempRepository.save(otpTemp);
                }
            }
        }
    }
}
