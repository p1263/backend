package com.fpt.edu.projectthesis.service.implement;


import com.fpt.edu.projectthesis.entity.*;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.BarberWorkTableDTO;
import com.fpt.edu.projectthesis.model.SalonWorkTableDTO;
import com.fpt.edu.projectthesis.repository.BarberRepository;
import com.fpt.edu.projectthesis.repository.SalonWorkTableRepository;
import com.fpt.edu.projectthesis.repository.TimeWorkTableRepository;
import com.fpt.edu.projectthesis.repository.BarberWorkTableRepository;
import com.fpt.edu.projectthesis.service.WorkTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class WorkTableServiceImple implements WorkTableService {

    @Autowired
    private BarberWorkTableRepository barberWorkTableRepository;
    @Autowired
    private TimeWorkTableRepository timeWorkTableRepository;
    @Autowired
    private SalonWorkTableRepository salonWorkTableRepository;
    @Autowired
    private BarberRepository barberRepository;

    @Override
    public void insertNewBarberWorktable(Barber barber) {
        List<TimeWorkTable> timeIds= timeWorkTableRepository.getAllTimeWorkTable();
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        for(int i =0;i<3;i++){
            for (TimeWorkTable timeId: timeIds) {
                BarberWorkTable workTable = new BarberWorkTable();
                workTable.setTimeId(timeId.getTimeId());
                workTable.setBarberId(barber.getBarberId());
                cal.setTime(date);
                cal.add(Calendar.DATE, i);
                workTable.setDate(cal.getTime());
                workTable.setStatus(true);
                workTable.setSalonId(barber.getSalonId());
                barberWorkTableRepository.save(workTable);
            }
        }
    }

    @Override
    public void insertNewSalonWorktable(Salon salon) {
        List<TimeWorkTable> timeIds= timeWorkTableRepository.getAllTimeWorkTable();
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        for(int i =0;i<3;i++){
            for (TimeWorkTable timeId: timeIds) {
                SalonWorkTable workTable = new SalonWorkTable();
                workTable.setTimeId(timeId.getTimeId());
                workTable.setSalonId(salon.getSalonId());
                cal.setTime(date);
                cal.add(Calendar.DATE, i);
                workTable.setDate(cal.getTime());
                workTable.setSlot(barberRepository.getNumberOfBarberBySalonId(salon.getSalonId()));
                salonWorkTableRepository.save(workTable);
            }
        }
    }



    @Scheduled(cron = "0 1 0 * * ?")
    public void updateWorkTable() {
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        List<BarberWorkTable> yesterdays = barberWorkTableRepository.getWorkTablesByDateBefore(date);
        if(yesterdays.size() != 0){
            for(BarberWorkTable yesterday: yesterdays){
                cal.setTime(date);
                cal.add(Calendar.DATE, 2);
                yesterday.setStatus(true);
                yesterday.setBookingId(null);
                yesterday.setDate(cal.getTime());
                barberWorkTableRepository.save(yesterday);
            }
        }
        List<SalonWorkTable> previousDays = salonWorkTableRepository.getSalonWorkTableByDateBefore(date);
        if(yesterdays.size()!=0){
            for(SalonWorkTable previous: previousDays){
                cal.setTime(date);
                cal.add(Calendar.DATE, 2);
                previous.setSlot(barberRepository.getNumberOfBarberBySalonId(previous.getSalonId()));
                previous.setDate(cal.getTime());
                salonWorkTableRepository.save(previous);
            }
        }
    }

    private int index=1;
    private final SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
    @Scheduled(cron = "0 1,31 8-19 * * ?")
    public void disablePastWorktable() throws ParseException {
        if (index==21 ) index=1;
        long time= timeWorkTableRepository.getTimebyId(index).getTime();
        long date = formatter.parse(formatter.format(java.util.Calendar.getInstance().getTime())).getTime();
        if(date-time==60000){
            disableWorktable(index);
            index++;
        }
    }
    private void disableWorktable(int index){
        List<BarberWorkTable> barberWorkTables = barberWorkTableRepository.getBarberWorkTableByTimeId(index,java.util.Calendar.getInstance().getTime());
        for(BarberWorkTable barberWorkTable: barberWorkTables){
            barberWorkTable.setStatus(false);
            barberWorkTableRepository.save(barberWorkTable);
        }
        List<SalonWorkTable> salonWorkTables = salonWorkTableRepository.getSalonWorkTableByTimeId(index,java.util.Calendar.getInstance().getTime());
        for(SalonWorkTable salonWorkTable:salonWorkTables){
            salonWorkTable.setSlot(0);
            salonWorkTableRepository.save(salonWorkTable);
        }
    }
    @Override
    public void hidePastTimeId() {
        try {
            List<TimeWorkTable> timeWorkTableList = timeWorkTableRepository.getAllTimeWorkTable();
            long date = formatter.parse(formatter.format(java.util.Calendar.getInstance().getTime())).getTime();
            if(date>formatter.parse("18:30:00").getTime()){
                for(int i=1;i<=20;i++){
                    disableWorktable(i);
                }
                index=1;
                return;
            }
            for(TimeWorkTable t : timeWorkTableList){
                if (date<t.getTimeStamp().getTime()) {
                    index=t.getTimeId()-1;
                    break;
                }

            }
            for(int i=1;i<=index;i++){
                disableWorktable(i);
            }
            index++;
        }catch (ParseException e){
            e.printStackTrace();
        }
    }

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    @Override
    public List<BarberWorkTableDTO> getWorkTablesByBarberId(Integer barberId, Integer roleId) throws ParseException {
        List<BarberWorkTable> workTables1 = barberWorkTableRepository.getWorkTablesByBarberId(barberId);
        List<BarberWorkTableDTO> workTables = new ArrayList<>();
        int check=0;
        Date now = formatter.parse(formatter.format(Calendar.getInstance().getTime()));
        for(BarberWorkTable t : workTables1){
            Date date = timeWorkTableRepository.getTimebyId(t.getTimeId());
            String time = date.toString();
            BarberWorkTableDTO b = Mapper.toBarberWorkTableDTO(t);
            b.setTime(time);
            if(now.compareTo(date) < 0 && check==0) {
                if (roleId == 3 && b.getDate().compareTo(dateFormat.parse(dateFormat.format(java.util.Calendar.getInstance().getTime()))) == 0) {
                    if (!workTables.isEmpty()) {
                        if (workTables.get(workTables.size() - 1).getBookingId() == null) {
                            workTables.get(workTables.size() - 1).setStatus(true);
                            check = 1;
                        }
                    }
                }
            }
            workTables.add(b);
            if (now.compareTo(formatter.parse("13:00:00")) <= 0 && now.compareTo(formatter.parse("12:00:00"))>0
                    && b.getDate().compareTo(dateFormat.parse(dateFormat.format(java.util.Calendar.getInstance().getTime()))) == 0
                    && b.getTimeId()==9 && roleId==3){
                workTables.get(workTables.size() - 2).setStatus(false);
            }else
            if (now.compareTo(formatter.parse("19:00:00")) <= 0 && now.compareTo(formatter.parse("18:30:00"))>0
            && b.getDate().compareTo(dateFormat.parse(dateFormat.format(java.util.Calendar.getInstance().getTime()))) == 0
            && b.getTimeId()==20 && roleId==3){
                workTables.get(workTables.size() - 1).setStatus(true);
            }

        }
        return workTables;
    }

    @Override
    public List<SalonWorkTableDTO> getWorkTablesBySalonId(Integer salonId, Integer roleId) throws ParseException {
        List<SalonWorkTable> salonWorktable1 = salonWorkTableRepository.getSalonWorkTableBySalonId(salonId);
        List<SalonWorkTableDTO> salonWorktable = new ArrayList<>();
        int check=0;
        Date now = formatter.parse(formatter.format(Calendar.getInstance().getTime()));
        for(SalonWorkTable t : salonWorktable1){
            Date date = timeWorkTableRepository.getTimebyId(t.getTimeId());
            String time = date.toString();
            SalonWorkTableDTO s = Mapper.toSalonWorkTableDTO(t);
            s.setTime(time);
            if(now.compareTo(date) < 0 && check==0) {
                if (roleId == 3 && s.getDate().compareTo(dateFormat.parse(dateFormat.format(java.util.Calendar.getInstance().getTime()))) == 0) {
                    if (!salonWorktable.isEmpty()) {
                        List<Integer> list = barberWorkTableRepository.getFreeBarberIdForManager(t.getSalonId(),t.getTimeId()-1,t.getDate());
                        if(!list.isEmpty()){
                            salonWorktable.get(salonWorktable.size()-1).setSlot(list.size());
                            check=1;
                        }

                    }
                }
            }
            salonWorktable.add(s);
            if (now.compareTo(formatter.parse("13:00:00")) <= 0 && now.compareTo(formatter.parse("12:00:00"))>0
                    && s.getDate().compareTo(dateFormat.parse(dateFormat.format(java.util.Calendar.getInstance().getTime()))) == 0
                    && s.getTimeId()==9 && roleId==3){
                salonWorktable.get(salonWorktable.size() - 2).setSlot(0);
            }else
            if (now.compareTo(formatter.parse("19:00:00")) <= 0 && now.compareTo(formatter.parse("18:30:00"))>0
                    && s.getDate().compareTo(dateFormat.parse(dateFormat.format(java.util.Calendar.getInstance().getTime()))) == 0
                    && s.getTimeId()==20 && roleId==3){
                List<Integer> list = barberWorkTableRepository.getFreeBarberIdForManager(t.getSalonId(),t.getTimeId()-1,t.getDate());
                if(!list.isEmpty())
                salonWorktable.get(salonWorktable.size() - 1).setSlot(list.size());
            }
        }
        return salonWorktable;

    }

    @Override
    public void checkWorktableWhenTurnOnServer() {
        if(barberWorkTableRepository.getListBarberWorktable().size()==0) return;
        Calendar cal = Calendar.getInstance();
        Date date1 = cal.getTime();
        List<BarberWorkTable> workTables = barberWorkTableRepository.getWorkTablesByDate(date1);
        TimeUnit time = TimeUnit.DAYS;

        if(workTables.size()==0){
            Date date = barberWorkTableRepository.minWorktableDate();
            Date today = java.util.Calendar.getInstance().getTime();
            long diff = today.getTime() - date.getTime();
            long difference = time.convert(diff, TimeUnit.MILLISECONDS);
            List<BarberWorkTable> yesterdays = barberWorkTableRepository.getWorkTablesByDateBefore(date1);
            if(yesterdays.size() != 0){
                for(BarberWorkTable yesterday: yesterdays){
                    cal.setTime(yesterday.getDate());
                    cal.add(Calendar.DATE, (int) difference);
                    yesterday.setStatus(true);
                    yesterday.setBookingId(null);
                    yesterday.setDate(cal.getTime());
                    barberWorkTableRepository.save(yesterday);
                }
            }
        }else{
            List<BarberWorkTable> yesterdays = barberWorkTableRepository.getWorkTablesByDateBefore(date1);
            if(yesterdays.size() != 0){
                for(BarberWorkTable yesterday: yesterdays){
                    yesterday.setStatus(true);
                    yesterday.setBookingId(null);
                    cal.setTime(yesterday.getDate());
                    cal.add(Calendar.DATE,3);
                    yesterday.setDate(cal.getTime());
                    barberWorkTableRepository.save(yesterday);
                }
            }
        }
        List<SalonWorkTable> workTables1 = salonWorkTableRepository.getSalonWorkTableByDateBefore(date1);
        if(workTables1.size()==0){
            Date date = salonWorkTableRepository.getMinSalonWorktabledate();
            Date today = java.util.Calendar.getInstance().getTime();
            long diff = today.getTime() - date.getTime();
            long difference = time.convert(diff, TimeUnit.MILLISECONDS);
            List<SalonWorkTable> yesterdays = salonWorkTableRepository.getSalonWorkTableByDateBefore(date1);
            if(yesterdays.size()!=0){
                for(SalonWorkTable yesterday: yesterdays){
                    cal.setTime(yesterday.getDate());
                    cal.add(Calendar.DATE, (int) difference);
                    yesterday.setSlot(barberRepository.getNumberOfBarberBySalonId(yesterday.getSalonId()));
                    yesterday.setDate(cal.getTime());
                    salonWorkTableRepository.save(yesterday);
                }
            }
        }else{
            List<SalonWorkTable> yesterdays = salonWorkTableRepository.getSalonWorkTableByDateBefore(date1);
            if(yesterdays.size()!=0){
                for(SalonWorkTable yesterday: yesterdays){
                    cal.setTime(yesterday.getDate());
                    cal.add(Calendar.DATE, 3);
                    yesterday.setSlot(barberRepository.getNumberOfBarberBySalonId(yesterday.getSalonId()));
                    yesterday.setDate(cal.getTime());
                    salonWorkTableRepository.save(yesterday);
                }
            }
        }
    }




}
