package com.fpt.edu.projectthesis.service;

import com.fpt.edu.projectthesis.model.ManagerDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;

public interface ManagerService {

    ManagerDTO createManager(ManagerDTO managerDTO) throws ParseException;

    boolean updateManagerInfo(ManagerDTO managerDTO) throws ParseException;

    ManagerDTO insertAvatarToManager(String phone, MultipartFile avatar) throws IOException, ParseException;

    ManagerDTO getManagerDetailByPhone(String phone) throws ParseException;

    ManagerDTO getManagerDetailBySalonId(Integer salonId) throws ParseException;

    ManagerDTO getManagerDetailByManagerId(Integer managerId) throws ParseException;
}
