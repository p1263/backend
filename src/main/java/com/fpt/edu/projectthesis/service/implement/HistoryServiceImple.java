package com.fpt.edu.projectthesis.service.implement;

import com.fpt.edu.projectthesis.entity.BookedService;
import com.fpt.edu.projectthesis.entity.Booking;
import com.fpt.edu.projectthesis.entity.History;
import com.fpt.edu.projectthesis.entity.SalonWork;
import com.fpt.edu.projectthesis.mapper.Mapper;
import com.fpt.edu.projectthesis.model.BookedServiceDTO;
import com.fpt.edu.projectthesis.model.BookingDTO;
import com.fpt.edu.projectthesis.model.HistoryDTO;
import com.fpt.edu.projectthesis.model.SalonWorkDTO;
import com.fpt.edu.projectthesis.repository.*;
import com.fpt.edu.projectthesis.service.BookedWorkService;
import com.fpt.edu.projectthesis.service.BookingService;
import com.fpt.edu.projectthesis.service.HistoryService;
import com.fpt.edu.projectthesis.service.SalonWorkService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class HistoryServiceImple implements HistoryService {

    @Autowired
    private HistoryRepositoty historyRepositoty;

    @Autowired
    private BarberRepository barberRepository;

    @Autowired
    private BookedWorkService bookedWorkService;

    @Autowired
    private SalonRepository salonRepository;

    @Autowired
    private SalonWorkService salonWorkService;

    @Override
    public List<HistoryDTO> getListHistoryByCustomerId(Integer customer_id) {
        List<History> listHistory = historyRepositoty.findHistoryByCustomerId(customer_id);
        if (listHistory != null) {
            List<HistoryDTO> listHistoryDTO = new ArrayList<>();
            //Get All History
            //Get All service for each history by booking id
            for (History history : listHistory) {
                HistoryDTO historyDTO = Mapper.toHistoryDTO(history);
                historyDTO.setSalon_name(salonRepository.getSalonNameBySalonId(history.getSalon_id()));
                historyDTO.setBarber_name(barberRepository.getBarberNameByBarberId(historyDTO.getBarber_id())); // Get Name of Barber by BarberId
                List<SalonWorkDTO> listSalonWorkDTO = new ArrayList<>();
                List<BookedServiceDTO> listBookedServiceDTO = bookedWorkService.getListServiceByBookingId(history.getBooking_id());
                for (BookedServiceDTO bookedServiceDTO : listBookedServiceDTO) {
                    SalonWorkDTO salonWorkDTO = salonWorkService.getSalonWorkById(bookedServiceDTO.getServiceId());
                    listSalonWorkDTO.add(salonWorkDTO);
                }
                historyDTO.setListService(listSalonWorkDTO);
                listHistoryDTO.add(historyDTO);
            }
            return listHistoryDTO;
        }
        return null;
    }

    @Override
    public HistoryDTO getHistoryById(Integer id) {
        Optional<History> historyOptional = historyRepositoty.findById(id);
        if(historyOptional.isPresent()){
            return Mapper.toHistoryDTO(historyOptional.get());
        }
        return null;
    }

    @Override
    public boolean createHistory(BookingDTO bookingDTO) throws ParseException {
        History history = new History();
        history.setBooking_id(bookingDTO.getBookingId());
        history.setBarber_id(bookingDTO.getBarberId());
        history.setCustomer_id(bookingDTO.getCustomerId());
        history.setSalon_id(bookingDTO.getSalonId());
        history.setPrice(bookingDTO.getPrice());
        Date now = new Date(System.currentTimeMillis());
        history.setDate(now);
        history.setStar(bookingDTO.getStar());
        history.setFrontPic(bookingDTO.getFrontPic());
        history.setLeftPic(bookingDTO.getLeftPic());
        history.setBehindPic(bookingDTO.getBehindPic());
        history.setRightPic(bookingDTO.getRightPic());
        History historyResult = historyRepositoty.save(history);
        if (historyResult != null) return true;
        return false;
    }

    @Override
    public HistoryDTO getLatestHistoryOfCustomer(Integer customerId) {
        Optional<History> historyOptional = historyRepositoty.getLatestHistoryByCustomerId(customerId);
        if(historyOptional.isPresent()){
            HistoryDTO historyDTO = Mapper.toHistoryDTO(historyOptional.get());
            historyDTO.setSalon_name(salonRepository.getSalonNameBySalonId(historyDTO.getSalon_id()));
            return historyDTO;
        }
        return null;
    }

    @Override
    public boolean updateStarToHistory(Integer bookingId, Integer star) {
        Optional<History> historyOptional = historyRepositoty.getHistoryByByBookingId(bookingId);
        if(historyOptional.isPresent()){
            Integer updateResult = historyRepositoty.updateStarOfHistoryByBookingId(star,bookingId);
            if(updateResult > 0) log.info("Customer rate successfully");
            return true;
        }
        return false;
    }
}
