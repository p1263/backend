package com.fpt.edu.projectthesis.mapper;

import com.fpt.edu.projectthesis.entity.*;
import com.fpt.edu.projectthesis.model.*;

public class Mapper {
    public static AccountDTO toAccountDTO(Account account) {
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(account.getId());
        accountDTO.setPhone(account.getPhone());
        accountDTO.setPassword(account.getPassword());
        accountDTO.setRoleId(account.getRoleId());
        return accountDTO;
    }

    public static RoleDTO toRoleDTO(Role role) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(role.getId());
        roleDTO.setName(role.getName());
        return roleDTO;
    }

    public static CustomerDTO toCustomerDTO(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(customer.getId());
        customerDTO.setName(customer.getName());
        customerDTO.setDob(customer.getDob());
        customerDTO.setEmail(customer.getEmail());
        customerDTO.setAddress(customer.getAddress());
        customerDTO.setAvatar(customer.getAvatar());
        customerDTO.setPhone(customer.getPhone());
        customerDTO.setRoleId(customer.getRoleId());
        customerDTO.setTotal_purchase(customer.getTotal_purchase());
        customerDTO.setRank_id(customer.getRank_id());
        return customerDTO;
    }

    public static SalonWorkDTO toSalonWorkDTO(SalonWork salonWork) {
        return new SalonWorkDTO(salonWork.getServiceId(), salonWork.getName(),
                salonWork.getPrice(), salonWork.getDescription(), salonWork.getImage(), salonWork.getPoint(),salonWork.isStatus());

    }

    public static BarberDTO toBarberDTO(Barber barber) {
        return new BarberDTO(barber.getBarberId(), barber.getName(), barber.getCMND(), barber.getEmail(), barber.getDOB(), barber.getPhone(),
                barber.getAvatar(), barber.getSalonId(), barber.getRoleId(), barber.getDayEnter(), barber.getBasePoint(), barber.getRankPoint(), barber.getBarberId());
    }

    public static Barber toBarber(BarberDTO barberDTO) {
        return new Barber(barberDTO.getName(), barberDTO.getCMND(), barberDTO.getEmail(), barberDTO.getDOB(), barberDTO.getPhone(),
                barberDTO.getAvatar(), barberDTO.getSalonId());
    }

    public static SalonDTO toSalonDTO(Salon salon) {
        SalonDTO salonDTO = new SalonDTO();
        salonDTO.setSalonId(salon.getSalonId());
        salonDTO.setName(salon.getName());
        salonDTO.setAddress(salon.getAddress());
        salonDTO.setManagerId(salon.getManagerId());
        salonDTO.setLatLocation(salon.getLatLocation());
        salonDTO.setLngLocation(salon.getLngLocation());
        salonDTO.setAvatar(salon.getAvatar());
        return salonDTO;
    }

    public static HistoryDTO toHistoryDTO(History history) {
        HistoryDTO historyDTO = new HistoryDTO();
        historyDTO.setId(history.getId());
        historyDTO.setCustomer_id(history.getCustomer_id());
        historyDTO.setBarber_id(history.getBarber_id());
        historyDTO.setPrice(history.getPrice());
        historyDTO.setBooking_id(history.getBooking_id());
        historyDTO.setSalon_id(history.getSalon_id());
        historyDTO.setStar(history.getStar());
        historyDTO.setFrontPic(history.getFrontPic());
        historyDTO.setLeftPic(history.getLeftPic());
        historyDTO.setBehindPic(history.getBehindPic());
        historyDTO.setRightPic(history.getRightPic());
        historyDTO.setDate(history.getDate());
        return historyDTO;
    }

    public static BarberWorkTableDTO toBarberWorkTableDTO(BarberWorkTable workTable){
        BarberWorkTableDTO barberWorkTableDTO = new BarberWorkTableDTO();
        barberWorkTableDTO.setId(workTable.getId());
        barberWorkTableDTO.setBarberId(workTable.getBarberId());
        barberWorkTableDTO.setSalonId(workTable.getBookingId());
        barberWorkTableDTO.setTimeId(workTable.getTimeId());
        barberWorkTableDTO.setDate(workTable.getDate());
        barberWorkTableDTO.setStatus(workTable.isStatus());
        barberWorkTableDTO.setSalonId(workTable.getSalonId());
        return barberWorkTableDTO;
    }



    public static BookingDTO toBookingDTO(Booking booking) {
        BookingDTO bookingDTO = new BookingDTO();
        bookingDTO.setBookingId(booking.getBookingId());
        bookingDTO.setSalonId(booking.getSalonId());
        bookingDTO.setBarberId(booking.getBarberId());
        bookingDTO.setCustomerId(booking.getCustomerId());
        bookingDTO.setDate(booking.getDate());
        bookingDTO.setStar(booking.getStar());
        bookingDTO.setTimeId(booking.getTimeId());
        bookingDTO.setFrontPic(booking.getFrontPic());
        bookingDTO.setLeftPic(booking.getLeftPic());
        bookingDTO.setBehindPic(booking.getBehindPic());
        bookingDTO.setRightPic(booking.getRightPic());
        bookingDTO.setStatus(booking.getStatus());
        return bookingDTO;
    }

    public static BookingDTO toBookingDTOWithoutImage(Booking booking){
        return new BookingDTO(booking.getBookingId(),booking.getCustomerId(),
                booking.getSalonId(),booking.getBarberId(),booking.getTimeId(),booking.getDate(),booking.getStatus());

    }

    public static Booking toBooking(BookingDTO bookingDTO) {
        return new Booking(bookingDTO.getCustomerId(), bookingDTO.getSalonId(), bookingDTO.getBarberId(), bookingDTO.getTimeId(), bookingDTO.getDate());
    }



    public static BookedService toBookedService(BookedServiceDTO bookedServiceDTO) {
        return new BookedService(bookedServiceDTO.getBookingId(), bookedServiceDTO.getServiceId());
    }

    public static BookedServiceDTO toBookedServiceDTO(BookedService bookedService) {
        return new BookedServiceDTO(bookedService.getBookingId(), bookedService.getServiceId());
    }

    public static SalonWorkTableDTO toSalonWorkTableDTO(SalonWorkTable salonWorkTable){
        SalonWorkTableDTO salonWorkTableDTO = new SalonWorkTableDTO();
        salonWorkTableDTO.setId(salonWorkTable.getId());
        salonWorkTableDTO.setSalonId(salonWorkTable.getSalonId());
        salonWorkTableDTO.setTimeId(salonWorkTable.getTimeId());
        salonWorkTableDTO.setSlot(salonWorkTable.getSlot());
        salonWorkTableDTO.setDate(salonWorkTable.getDate());
        return salonWorkTableDTO;
    }

    public static Salon toSalon(SalonDTO salonDTO){
        return new Salon(salonDTO.getSalonId(), salonDTO.getName(), salonDTO.getAddress(),
                salonDTO.getManagerId(), salonDTO.getLatLocation(), salonDTO.getLngLocation(), salonDTO.getAvatar());
    }

    public static ManagerDTO toManagerDTO(Manager manager){
        ManagerDTO managerDTO = new ManagerDTO();
        managerDTO.setId(manager.getId());
        managerDTO.setCmnd(manager.getCmnd());
        managerDTO.setPhone(manager.getPhone());
        managerDTO.setAvatar(manager.getAvatar());
        managerDTO.setSalonId(manager.getSalonId());
        managerDTO.setName(manager.getName());
        managerDTO.setDob(managerDTO.getDob());
        managerDTO.setRoleId(manager.getRoleId());
        return managerDTO;
    }

    public static AdminDTO toAdminDTO(Admin admin){
        AdminDTO adminDTO = new AdminDTO();
        adminDTO.setId(admin.getId());
        adminDTO.setCmnd(admin.getCmnd());
        adminDTO.setName(admin.getName());
        adminDTO.setPhone(admin.getPhone());
        adminDTO.setAvatar(admin.getAvatar());
        adminDTO.setDob(admin.getDob());
        adminDTO.setRoleId(admin.getRoleId());
        return adminDTO;
    }
}
