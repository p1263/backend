package com.fpt.edu.projectthesis.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.service.WorkTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import com.fpt.edu.projectthesis.constant.ResponseStatus;

import javax.persistence.EntityManager;
import java.text.ParseException;


@Service
@RequestMapping("/api/worktable")
public class WorkTableController extends BaseController{
    @Autowired
    private WorkTableService workTableService;


    @GetMapping ("/barber/{roleId}/{barberId}")
    public ResponseEntity<?> getBarberWorktable(@PathVariable("barberId")Integer barberId,@PathVariable("roleId") Integer roleId) throws ParseException {
        Object obj = workTableService.getWorkTablesByBarberId(barberId,roleId);
        int status = ResponseStatus.SUCCESS;
        String message;
        if(obj == null){
            message = "Không có dữ liệu";
        }else{
            message = "Gọi API thành công";
        }
        return toResponse(new MessageResponse(status,message,obj));
    }
    @GetMapping("/salon/{roleId}/{salonId}")
    public ResponseEntity<?> getSalonWorktable(@PathVariable("salonId")Integer salonId,@PathVariable("roleId") Integer roleId) throws ParseException {
        Object obj = workTableService.getWorkTablesBySalonId(salonId,roleId);
        int status = ResponseStatus.SUCCESS;
        String message;
        if(obj == null){
            message = "Không có dữ liệu";
        }else{
            message = "Gọi API thành công";
        }
        return toResponse(new MessageResponse(status,message,obj));
    }


}
