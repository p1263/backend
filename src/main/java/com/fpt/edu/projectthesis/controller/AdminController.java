package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.entity.Admin;
import com.fpt.edu.projectthesis.model.AdminDTO;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@Controller
@RequestMapping("/api/admin")
public class AdminController extends BaseController {

    @Autowired
    private AdminService adminService;

    @PostMapping("/create")
    public ResponseEntity<?> createAdmin(@RequestBody AdminDTO adminDTO) throws ParseException {
        AdminDTO adminDTOResult = adminService.createAdmin(adminDTO);
        int status = adminDTOResult == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = adminDTOResult == null ? "Khởi tạo admin thất bại" : "Khời tạo admin thành công";
        return toResponse(new MessageResponse(status,message,adminDTOResult));
    }
    @PostMapping("/update")
    public ResponseEntity<?> updateAdminInfo(@RequestBody AdminDTO adminDTO) throws ParseException {
        AdminDTO adminDTOResult  = adminService.updateInfoAdmin(adminDTO);
        int status = adminDTOResult == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = adminDTOResult == null ? "Cập nhật admin thất bại" : "Cập nhật admin admin thành công";
        return toResponse(new MessageResponse(status,message,adminDTOResult));
    }
    @GetMapping("/id/{adminId}")
    public ResponseEntity<?> getAdminInfoById(@PathVariable("adminId")Integer adminId){
        AdminDTO adminDTO = adminService.getInfoAdminById(adminId);
        int status = adminDTO == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = adminDTO == null ? "Lấy dữ liệu admin thất bại": "Lấy dữ liệu admin thành công";
        return toResponse(new MessageResponse(status,message,adminDTO));
    }

}
