package com.fpt.edu.projectthesis.controller;


import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fpt.edu.projectthesis.entity.Salon;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.model.SalonDTO;
import com.fpt.edu.projectthesis.service.SalonService;
import com.fpt.edu.projectthesis.service.WorkTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.fpt.edu.projectthesis.constant.ResponseStatus;

import java.util.List;

@Controller
@RequestMapping("/api/salon")
public class SalonController extends BaseController {

    @Autowired
    private SalonService salonService;

    @GetMapping("/list/free")
    public ResponseEntity<?> getListSalonNotHaveManager() {
        List<SalonDTO> listSalonDTO = salonService.getListSalonFree();
        int status = ResponseStatus.SUCCESS;
        String message = listSalonDTO == null ? "Không có salon nào đang không có quản lý" : "Lấy danh sách salon không có quản lý thành công";
        return toResponse(new MessageResponse(status, message, listSalonDTO));
    }


    @GetMapping("/getList")
    public ResponseEntity<?> getListSalon() {
        Object obj = salonService.getListSalon();
        int status = ResponseStatus.SUCCESS;
        String message = "Lấy danh sách salon thành công";
        return toResponse(new MessageResponse(status, message, obj));
    }

    @PostMapping("")
    public ResponseEntity<?> getSalon(@RequestBody SalonDTO salonDTO) {
        Object obj = salonService.getSalonById(salonDTO.getSalonId());

        int status = ResponseStatus.SUCCESS;
        String message;
        if (obj == null) {
            message = "Không có dữ liệu";
        } else {
            message = "Lấy dữ liệu salon thành công";
        }
        return toResponse(new MessageResponse(status, message, obj));
    }

    @PostMapping("/add")
    public ResponseEntity<?> addSalon(@RequestBody ObjectNode json) {
        try {
            SalonDTO salonDTO = new SalonDTO();
            salonDTO.setAddress(json.get("address").asText());
            salonDTO.setLatLocation(Double.parseDouble(json.get("lat").asText()));
            salonDTO.setLngLocation(Double.parseDouble(json.get("lng").asText()));
            salonDTO.setName(json.get("name").asText());
            salonDTO.setAvatar(json.get("avatar").asText());
            return toResponse(new MessageResponse(ResponseStatus.SUCCESS, salonService.addNewSalon(salonDTO), salonDTO));
        } catch (NumberFormatException ex) {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Kinh độ hoặc vĩ độ không hợp lệ", null));
        }
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateSalon(@RequestBody SalonDTO salonDTO) {
        boolean isSuccess = salonService.updateSalonInfo(salonDTO);
        int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
        String message = isSuccess == true ? "Cập nhật salon thành công" : " Cập nhật salon thất bại";
        return toResponse(new MessageResponse(status, message, null));

    }
}
