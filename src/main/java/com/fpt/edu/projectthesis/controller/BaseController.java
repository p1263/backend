package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.model.MessageResponse;
import org.springframework.http.ResponseEntity;

public class BaseController {
    public ResponseEntity<?> toResponse(Object object){
        if(object == null){
            return ResponseEntity.ok(new MessageResponse(ResponseStatus.FAIL, "Call API Failed", null));
        }else{
            return ResponseEntity.ok(object);
        }
    }
}
