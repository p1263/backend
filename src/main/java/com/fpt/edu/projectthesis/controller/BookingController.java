package com.fpt.edu.projectthesis.controller;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.entity.Customer;
import com.fpt.edu.projectthesis.model.BarberDTO;
import com.fpt.edu.projectthesis.model.BookedServiceDTO;
import com.fpt.edu.projectthesis.model.BookingDTO;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.entity.Booking;
import com.fpt.edu.projectthesis.model.*;
import com.fpt.edu.projectthesis.service.*;
import com.google.api.client.json.Json;
import com.google.firebase.messaging.FirebaseMessagingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api/booking")
public class BookingController extends BaseController {

    @Autowired
    private BookingService bookingService;

    @Autowired
    private BarberService barberService;

    @Autowired
    private ValidateService validateService;

    private final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

    @PostMapping("/book")
    public ResponseEntity<?> book(@RequestBody ObjectNode json) throws ParseException, FirebaseMessagingException {

        JsonNode services = json.get("service");
        BookingDTO bookingDTO = new BookingDTO();
        if (json.get("barberId") != null) {
            bookingDTO.setBarberId(Integer.parseInt(json.get("barberId").asText()));
        }

        String date1 = json.get("date").asText();
        Date date = formatter.parse(date1);
        bookingDTO.setDate(date);
        bookingDTO.setSalonId(Integer.parseInt(json.get("salonId").asText()));
        bookingDTO.setCustomerId(Integer.parseInt(json.get("userId").asText()));
        bookingDTO.setTimeId(Integer.parseInt(json.get("timeId").asText()));
        List<BookedServiceDTO> bookedServiceDTOList = new ArrayList<>();
        for (JsonNode service : services) {
            BookedServiceDTO bookedServiceDTO = new BookedServiceDTO(0, Integer.parseInt(service.asText()));
            bookedServiceDTOList.add(bookedServiceDTO);
        }
        bookingDTO.setListBookedService(bookedServiceDTOList);
        Object result = bookingService.insertNewBooking(bookingDTO,1);
        if (result instanceof String) {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, (String) result, null));
        }
        BookingDTO booking = (BookingDTO) result;

        booking = bookingService.getServiceName(booking);
        return toResponse(new MessageResponse(ResponseStatus.SUCCESS, "Đặt lịch thành công", booking));
    }

    @PostMapping("/manager")
    public ResponseEntity<?> managerBook(@RequestBody ObjectNode json) throws ParseException, FirebaseMessagingException {
        JsonNode services = json.get("service");
        BookingDTO bookingDTO = new BookingDTO();
        if (json.get("barberId") != null) {
            bookingDTO.setBarberId(Integer.parseInt(json.get("barberId").asText()));
        }
        String date1 = json.get("date").asText();
        Date date = formatter.parse(date1);
        bookingDTO.setDate(date);
        bookingDTO.setSalonId(Integer.parseInt(json.get("salonId").asText()));
        bookingDTO.setTimeId(Integer.parseInt(json.get("timeId").asText()));
        List<BookedServiceDTO> bookedServiceDTOList = new ArrayList<>();
        for (JsonNode service : services) {
            BookedServiceDTO bookedServiceDTO = new BookedServiceDTO(0, Integer.parseInt(service.asText()));
            bookedServiceDTOList.add(bookedServiceDTO);
        }
        bookingDTO.setListBookedService(bookedServiceDTOList);
        Object result;
        if(json.get("userId")!=null) {
            bookingDTO.setCustomerId(Integer.parseInt(json.get("userId").asText()));
             result = bookingService.managerBookingExistedCustomer(bookingDTO);
        }
        else {
            if(!validateService.isValidPhoneAndNotNull(json.get("customerPhone").asText())){
                return toResponse(new MessageResponse(ResponseStatus.FAIL,"Số điện thoại không hợp lệ",null));
            }
            Customer customer = new Customer();
            customer.setName(json.get("customerName").asText());
            customer.setPhone(json.get("customerPhone").asText());
             result=bookingService.managerBooking(bookingDTO,customer);
        }
        if (result instanceof String) {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, (String) result, null));
        }
        BookingDTO booking = (BookingDTO) result;
        booking = bookingService.getServiceName(booking);
        return toResponse(new MessageResponse(ResponseStatus.SUCCESS, "Đặt lịch thành công", booking));
    }

    @PostMapping("/cancel")
    public ResponseEntity<?> cancelBook(@RequestBody ObjectNode json) throws ParseException, FirebaseMessagingException {
        if (json.get("bookingId") == null) {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Huỷ lịch thất bại", null));
        }
        String result = bookingService.cancelBooking(Integer.parseInt(json.get("bookingId").asText()));
        if (result.equals("")) {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Huỷ lịch thất bại", null));
        } else return toResponse(new MessageResponse(ResponseStatus.SUCCESS, result, null));
    }

    @GetMapping("/customer/{customerId}")
    public ResponseEntity<?> getCustomerBooked(@PathVariable("customerId") Integer customerId) {
        if (customerId == null) {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Lấy lịch thất bại", null));
        }
        Object booking = bookingService.getBookedByCustomerId(customerId);
        if (booking instanceof String) {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, (String) booking, null));
        }
        return toResponse(new MessageResponse(ResponseStatus.SUCCESS, "Lấy lịch thành công", booking));
    }

    //Truyền vào booking ID để lấy thông tin chi tiết của booking
    @GetMapping("/detail/{bookingId}")
    public ResponseEntity<?> getBookingDetail(@PathVariable("bookingId") Integer bookingId) {
        BookingDTO bookingDTO = bookingService.getBookingDTODetailByBookingId(bookingId);
        int status = bookingDTO != null ? 0 : 1;
        String message = bookingDTO != null ? "Lấy dữ liệu booking theo bookingId thành công" : "Lấy dữ liệu booking theo bookingId không thành công";
        return toResponse(new MessageResponse(status, message, bookingDTO));
    }

    //Truyền vào biến salonId để lấy danh sách booking theo ngày
    @PostMapping("/detail/all")
    public ResponseEntity<?> getAllBookingOfDay(@RequestBody ObjectNode json) throws ParseException {
        String salonIdRaw = json.get("salonId").asText();
        String customerPhone = json.get("phone").asText();
        if (validateService.isValidNumber(salonIdRaw)) {
            if (validateService.isValidPhoneAllowNull(customerPhone)) {
                Integer salonId = Integer.parseInt(salonIdRaw);
                Object listBookingDTO = bookingService.getAllListBookingByDateAndPhone(salonId, customerPhone);
                if(listBookingDTO instanceof String){
                    return toResponse(new MessageResponse(ResponseStatus.FAIL,(String)listBookingDTO,null));
                }
                return toResponse(new MessageResponse(ResponseStatus.SUCCESS, "Lấy dữ liệu booking thành công", listBookingDTO));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không hợp lệ", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "salonId không hợp lệ", null));
    }

    //Truyền vào biến time id, barberid, lấy danh sách barber đang rảnh.
    @PostMapping("/listBarberFree")
    public ResponseEntity<?> getListBarberFreeInTimeId(@RequestBody BookingDTO bookingDTO) {
        List<BarberDTO> listbarberDTO = barberService.getListBarberFreeOfSlot(bookingDTO.getSalonId(), bookingDTO.getTimeId(), bookingDTO.getDate());
        if (listbarberDTO != null) {
            int status = ResponseStatus.SUCCESS;
            String message = listbarberDTO.isEmpty() ? "Hiện tại không có barber rảnh" : "Lấy danh sách barber rảnh thành công";
            return toResponse(new MessageResponse(status, message, listbarberDTO));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Lấy danh sách barber rảnh thất bại", null));
    }

    @PostMapping("/barber")
    public ResponseEntity<?> getListWorkOrDayForBarber(@RequestBody JsonNode json) {
        Integer barberId = Integer.parseInt(json.get("barberId").asText());
        String status="";
        if(json.get("status") !=null)
            status = json.get("status").asText();
        int page = Integer.parseInt(json.get("page").asText());

        List<BookingDTO> bookingDTOS = bookingService.getListWorkOfDayForBarber(barberId,page,status);
        if(bookingDTOS==null){
            return toResponse(new MessageResponse(ResponseStatus.FAIL,"Bạn chưa có khách đặt cho hôm nay",null));
        }
        return toResponse(new MessageResponse(ResponseStatus.SUCCESS,"Lịch làm việc của bạn",bookingDTOS));
    }

    @PostMapping(value = "/status/finishServe")
    public ResponseEntity<?> confirmFinishServe(@RequestBody BookingDTO bookingDTOraw) throws IOException, ParseException, FirebaseMessagingException {

        if (bookingDTOraw.getFrontPic() != null && !bookingDTOraw.getFrontPic().isEmpty() &&
                bookingDTOraw.getLeftPic() != null && !bookingDTOraw.getLeftPic().isEmpty() &&
                bookingDTOraw.getBehindPic() != null && !bookingDTOraw.getBehindPic().isEmpty() &&
                bookingDTOraw.getRightPic() != null && ! bookingDTOraw.getRightPic().isEmpty()) {
            BookingDTO bookingDTO = bookingService.insertImagesToBooking(bookingDTOraw.getBookingId(),
                    bookingDTOraw.getFrontPic(),
                    bookingDTOraw.getLeftPic(),
                    bookingDTOraw.getBehindPic(),
                    bookingDTOraw.getRightPic());
            int status = bookingDTO == null ? 1 : 0;
            String message = bookingDTO == null ? "Xác nhận hoàn thành dịch vụ thất bại" : "Xác nhận hoàn thành dịch vụ thành công";
            return toResponse(new MessageResponse(status, message, bookingDTO));
        } else if (bookingDTOraw.getFrontPic() == null && bookingDTOraw.getLeftPic() == null && bookingDTOraw.getBehindPic() == null && bookingDTOraw.getRightPic() == null ||
                bookingDTOraw.getFrontPic().isEmpty() && bookingDTOraw.getLeftPic().isEmpty() && bookingDTOraw.getBehindPic().isEmpty() && bookingDTOraw.getRightPic().isEmpty()) {
            BookingDTO bookingDTO = bookingService.insertImagesToBooking(bookingDTOraw.getBookingId(), null, null, null, null);
            int status = bookingDTO == null ? 1 : 0;
            String message = bookingDTO == null ? "Khách không đồng ý chụp ảnh - Đổi trạng thái booking thất bại" : "Khách không đồng ý chụp ảnh - Dổi trạng thái booking thành công";
            return toResponse(new MessageResponse(status, message, bookingDTO));
        } else {
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Vui lòng chụp đủ ảnh trước - trái - sau - phải", null));
        }
    }

    @PostMapping("/status/arrived")
    public ResponseEntity<?> confirmCustomerArrived(@RequestBody ObjectNode json) throws FirebaseMessagingException, ParseException {
        if (!json.isEmpty() || json != null) {
            if (validateService.isValidNumber(json.get("bookingId").asText())) {
                if (validateService.isValidNumber(json.get("barberId").asText())) {
                    if (json.get("services") != null && !json.get("services").isEmpty()) {
                        Integer bookingId = Integer.parseInt(json.get("bookingId").asText());
                        Integer barberId = Integer.parseInt(json.get("barberId").asText());
                        JsonNode services = json.get("services");
                        boolean isSuccess = bookingService.confirmCustomerArrived(bookingId, barberId, services);
                        int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
                        String message = isSuccess == true ? "Xác nhận khách đến thành công" : "Xác nhận khách đến thất bại";
                        return toResponse(new MessageResponse(status, message, null));
                    }
                    return toResponse(new MessageResponse(ResponseStatus.FAIL, "Service phải khác rỗng", null));
                }
                return toResponse(new MessageResponse(ResponseStatus.FAIL, "BarberId phải khác rỗng và là số", null));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "BookingId phải khác rỗng và là số", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Cần truyền thông tin", null));


    }

    @PostMapping("/status/serving")
    public ResponseEntity<?> confirmServingCustomer(@RequestBody BookingDTO bookingDTO) throws FirebaseMessagingException {
        boolean isSuccess = bookingService.confirmServingCustomer(bookingDTO.getBookingId());
        int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
        String message = isSuccess == true ? "Xác nhận bắt đầu phục vụ khách hàng thành công" : "Xác nhận bắt đầu phục vụ khách hàng thất bại";
        return toResponse(new MessageResponse(status, message, null));
    }

    @PostMapping("/status/completed")
    public ResponseEntity<?> confirmCompleted(@RequestBody BookingDTO bookingDTO) throws ParseException, FirebaseMessagingException {
        BookingDTO bookingDTOResult = bookingService.confirmComplete(bookingDTO.getBookingId());
        int status = bookingDTOResult == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = bookingDTOResult == null ? "Cập nhật trạng thái booking thất bại" : "Thanh toán thành công";
        return toResponse(new MessageResponse(status, message, bookingDTOResult));
    }

    @PostMapping("/status/billing")
    public ResponseEntity<?> getListBillingOfSalon(@RequestBody BookingDTO bookingDTO){
        List<BookingDTO> listBookingDTO = bookingService.getAllListBillingOfSalon(bookingDTO.getSalonId());
        int status = listBookingDTO == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = listBookingDTO == null? "Lấy danh sách billing thất bại" : "Lấy danh sách billing thành công";
        return toResponse(new MessageResponse(status,message,listBookingDTO));
    }

    @PostMapping("/status")
    public ResponseEntity<?> getListBookingBySalonIdAndStatus(@RequestBody JsonNode json){
        Integer salonId = Integer.parseInt(json.get("salonId").asText());
        String status1 = "";
        if(json.get("status") !=null){
            status1=json.get("status").asText();
        }
        int page = Integer.parseInt(json.get("page").asText());
        Object data = bookingService.getAllListBookingBySalonIdAndStatus(salonId, status1,page);
        int status = data == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = data == null ? "Lấy danh sách booking thất bại" : "Lấy danh sách booking thành công";
        return toResponse(new MessageResponse(status, message,data));
    }

    @PostMapping("/rating")
    public ResponseEntity<?> customerRating(@RequestBody BookingDTO bookingDTO){
        String data = bookingService.customerRating(bookingDTO.getBookingId(),bookingDTO.getStar());
        int status = data == null ? ResponseStatus.FAIL:ResponseStatus.SUCCESS;
        String message = data == null ? "Booking không tồn tại": data;
        return toResponse(new MessageResponse(status,message,null));
    }

}
