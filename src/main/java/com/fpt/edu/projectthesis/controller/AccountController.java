package com.fpt.edu.projectthesis.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.model.AccountDTO;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.model.RoleDTO;
import com.fpt.edu.projectthesis.service.AccountService;
import com.fpt.edu.projectthesis.service.RoleService;
import com.fpt.edu.projectthesis.service.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.FORBIDDEN;

@Controller
@RequestMapping("/api/account")
public class AccountController extends BaseController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    AccountService accountService;

    @Autowired
    RoleService roleService;

    @Autowired
    ValidateService validateService;

    //Permit All
    @PostMapping("/register")
    public ResponseEntity<?> createAccount(@RequestBody AccountDTO accountDTO) throws ParseException {
        if (validateService.isValidPhoneAndNotNull(accountDTO.getPhone())) {
            if (validateService.isValidPassword(accountDTO.getPassword())) {
                boolean isSuccess = accountService.createAccount(accountDTO);
                int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
                String message = isSuccess == true ? "Tạo tài khoản thành công" : "Tạo tài khoản không thành công";
                accountDTO = accountService.getAccountDetailsByPhone(accountDTO.getPhone());
                return toResponse(new MessageResponse(status, message, isSuccess ? accountDTO : null));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Mật khẩu không hợp lệ", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không hợp lệ", null));
    }

    @GetMapping("/details/{phone}")
    public ResponseEntity<?> getAccountDetail(@PathVariable String phone) {
        if (validateService.isValidPassword(phone)) {
            return toResponse(new MessageResponse(ResponseStatus.SUCCESS, "Lấy thông tin tài khoản thành công", accountService.getAccountDetailsByPhone(phone)));
        } else
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Lấy thông tin tài khoản không thành công", null));
    }

    @GetMapping("/getRole")
    public ResponseEntity<?> getRoleToChange() {
        List<RoleDTO> listRoleDTO = roleService.getRoleCustomerAndBarber();
        int status = ResponseStatus.SUCCESS;
        String message = "";
        if (listRoleDTO.isEmpty() || listRoleDTO == null) {
            status = ResponseStatus.FAIL;
            message = "Lấy danh sách role không thành công";
            return toResponse(new MessageResponse(status, message, null));
        } else {
            message = "Lấy danh sách role thành công";
            return toResponse(new MessageResponse(status, message, listRoleDTO));
        }
    }

    //Permit All
    @PostMapping("/checkexist")
    public ResponseEntity<?> checkPhoneExist(@RequestBody AccountDTO accountDTO) {
        if (validateService.isValidPhoneAndNotNull(accountDTO.getPhone())) {
            boolean isSuccess = accountService.isAccountExist(accountDTO.getPhone());
            int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.SUCCESS;
            String message = isSuccess == true ? "0" : "1";
            return toResponse(new MessageResponse(status, message, null));
        } else return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không hợp lệ", null));
    }

    //Permit All
    @PostMapping("/chgpwd")
    public ResponseEntity<?> changePassword(@RequestBody AccountDTO accountDTO) {
        if (validateService.isValidPhoneAndNotNull(accountDTO.getPhone())) {
            if (validateService.isValidPassword(accountDTO.getPassword())) {
                boolean isSuccess = accountService.changePassword(accountDTO.getPassword(), accountDTO.getPhone());
                String message = isSuccess == true ? "Đổi mật khẩu thành công" : "Đổi mật khẩu thất bại";
                int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
                return toResponse(new MessageResponse(status, message, null));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Mật khẩu không hợp lệ", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không hợp lệ", null));
    }

    // Permit All
    @GetMapping("/token/refresh")
    public void refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String authorizationHeader = request.getHeader(AUTHORIZATION);
        if (authorizationHeader != null && authorizationHeader.startsWith("Ironcap ")) {
            try {
                String refreshToken = authorizationHeader.substring("Ironcap ".length());
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes());
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refreshToken);
                String username = decodedJWT.getSubject();
                AccountDTO accountDTO = accountService.getAccountDetailsByPhone(username);
                String[] roles = decodedJWT.getClaim("role").asArray(String.class);
                List<RoleDTO> listRole = new ArrayList<>();
                String accessToken = JWT.create()
                        .withSubject(accountDTO.getPhone())
                        .withExpiresAt(new Date(System.currentTimeMillis() + 20 * 60 * 1000))
                        .withIssuer(request.getRequestURL().toString())
                        .withClaim("role", listRole.add(roleService.findRoleById(accountDTO.getRoleId())))
                        .sign(algorithm);
                Map<String, String> tokens = new HashMap<>();
                tokens.put("status", "0");
                tokens.put("message", "Refresh Token Successfully");
                tokens.put("phone", username);
                tokens.put("accessToken", accessToken);
                tokens.put("refreshToken", refreshToken);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), tokens);
            } catch (Exception exception) {
                response.setHeader("error", exception.getMessage());
                response.setStatus(FORBIDDEN.value());
                Map<String, String> error = new HashMap<>();
                error.put("status", "1");
                error.put("message", "error");
                error.put("errorMessage", exception.getMessage());
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(), error);
            }
        } else {
            throw new RuntimeException("Refreshtoken is missing");
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(@RequestBody AccountDTO accountDTO) {
        if (validateService.isValidPhoneAndNotNull(accountDTO.getPhone())) {
            if (accountService.isAccountExist(accountDTO.getPhone())) {
                boolean isSuccess = accountService.logOut(accountDTO.getPhone());
                int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
                String message = isSuccess == true ? "Đăng xuất thành công" : "Đăng xuất thất bại";
                return toResponse(new MessageResponse(status, message, null));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không có trong hệ thống", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không đúng định dạng", null));
    }

    @PostMapping("/chgnewpwd")
    public ResponseEntity<?> changeNewPassword(@RequestBody AccountDTO accountDTO) {
        if (validateService.isValidPassword(accountDTO.getPassword())) {
            if (validateService.isValidPassword(accountDTO.getNewPassword())) {
                String isSuccess = accountService.changeNewPassword(accountDTO.getId(), accountDTO.getPassword(), accountDTO.getNewPassword());
                int status = isSuccess.equals("SUCCESS") ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
                String message = isSuccess.equals("SUCCESS") ? "Đổi mật khẩu thành công" : isSuccess;
                return toResponse(new MessageResponse(status, message, null));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Mật khẩu mới phải từ 8-12 ký tự", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Mật khẩu phải từ 8-12 ký tự", null));
    }

}
