package com.fpt.edu.projectthesis.controller;


import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.model.HistoryDTO;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.service.HistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/api/history")
@Slf4j
public class HistoryController extends BaseController {

    @Autowired
    private HistoryService historyService;
    @GetMapping("/{customerId}")
    public ResponseEntity<?> getAllHistoryOfCustomer(@PathVariable("customerId") Integer customerId){
        //Check id
        List<HistoryDTO> listHistoryDTO = historyService.getListHistoryByCustomerId(customerId);
        if(listHistoryDTO.isEmpty()){
            int status = 0;
            String message = "Quý khách chưa từng sử dụng dịch vụ tại Ironcap";
            return toResponse(new MessageResponse(status,message,null));
        }else{
            int status = 0;
            String message = "Lấy dữ liệu lịch sử thành công";
            return toResponse(new MessageResponse(status,message,listHistoryDTO));
        }
    }

    @GetMapping("/id/{historyId}")
    public ResponseEntity<?> getHistoryById(@PathVariable("historyId") Integer historyId){
        HistoryDTO historyDTO = historyService.getHistoryById(historyId);
        int status = historyDTO == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = historyDTO == null ? "Lấy lịch sử thất bại" : "Lấy lịch sử thành công";
        return toResponse(new MessageResponse(status,message,historyDTO));
    }
}
