package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.entity.RoleFunction;
import com.fpt.edu.projectthesis.model.*;
import com.fpt.edu.projectthesis.service.*;
import com.fpt.edu.projectthesis.service.implement.FirebaseMessagingService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/home")
public class HomeController extends BaseController {

    @Autowired
    private ManagerService managerService;

    @Autowired
    private AdminService adminService;

    @Autowired
    private ValidateService validateService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private BarberService barberService;

    @Autowired
    private RoleFunctionService roleFunctionService;

    @Autowired
    private HistoryService historyService;

    @PostMapping("")
    public ResponseEntity<?> getInfoHome(@RequestBody AccountDTO accountDTO) throws FirebaseMessagingException, ParseException {
        if (validateService.isValidPhoneAndNotNull(accountDTO.getPhone())) {
            if (accountDTO.getDeviceToken() != null && !accountDTO.getDeviceToken().isEmpty()) {
                AccountDTO accountDTOService = accountService.getAccountDetailsByPhone(accountDTO.getPhone());
                //insert device token to account
                accountService.insertDeviceTokenToAccount(accountDTO.getPhone(), accountDTO.getDeviceToken());
                HashMap<String, Object> data = new HashMap<>();
                int status = ResponseStatus.SUCCESS;
                String message = "";
                switch (accountDTOService.getRoleId()) {
                    case 1:
                        CustomerDTO customerDTO = customerService.getCustomerDetailByPhone(accountDTO.getPhone());
                        List<RoleFunction> customerRoleFunction = roleFunctionService.getAllFunctionOfRole(accountDTOService.getRoleId());
                        message = "Lấy thông tin home của customer: " + accountDTO.getPhone() + " thành công";
                        HistoryDTO historyDTO = historyService.getLatestHistoryOfCustomer(customerDTO.getId());
                        data.put("customer", customerDTO);
                        data.put("function", customerRoleFunction);
                        data.put("latest history",historyDTO);
                        break;
                    case 2:
                        BarberDTO barberDTO = barberService.getBarberByPhone(accountDTO.getPhone());
                        message = "Lấy thông tin home của barber: " + accountDTO.getPhone() + " thành công";
                        List<RoleFunction> barberRoleFunction = roleFunctionService.getAllFunctionOfRole(accountDTOService.getRoleId());
                        data.put("customer", barberDTO);
                        data.put("function", barberRoleFunction);
                        break;
                    case 3:
                        ManagerDTO managerDTO = managerService.getManagerDetailByPhone(accountDTO.getPhone());
                        message = "Lấy thông tin home của manager: " + accountDTO.getPhone() + " thành công";
                        List<RoleFunction> managerRoleFunction = roleFunctionService.getAllFunctionOfRole(accountDTOService.getRoleId());
                        data.put("customer", managerDTO);
                        data.put("function", managerRoleFunction);
                        break;
                    case 4:
                        AdminDTO adminDTO = adminService.getInfoAdminByPhone(accountDTO.getPhone());
                        message = "Lấy thông tin home của admin: " + accountDTO.getPhone() + " thành công";
                        List<RoleFunction> adminrRoleFunction = roleFunctionService.getAllFunctionOfRole(accountDTOService.getRoleId());
                        data.put("customer", adminDTO);
                        data.put("function", adminrRoleFunction);
                        break;
                }
                return toResponse(new MessageResponse(status, message, data));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Device Token không đúng", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không đúng định dạng", null));
    }
}
