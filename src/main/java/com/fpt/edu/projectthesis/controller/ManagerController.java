package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.model.BarberDTO;
import com.fpt.edu.projectthesis.model.ManagerDTO;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.service.BarberService;
import com.fpt.edu.projectthesis.service.ManagerService;
import lombok.extern.slf4j.Slf4j;
import org.junit.runner.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@Service
@Slf4j
@RequestMapping("/api/manager")
public class ManagerController extends BaseController {

    @Autowired
    private ManagerService managerService;

    @PostMapping("/create")
    public ResponseEntity<?> createManager(@RequestBody ManagerDTO managerDTO) throws ParseException {
        ManagerDTO managerDTOResult = managerService.createManager(managerDTO);
        int status = managerDTOResult == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = managerDTOResult == null ? "Tạo manager thất bại" : "Tạo manager thành công";
        return toResponse(new MessageResponse(status, message, managerDTOResult));
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateManager(@RequestBody ManagerDTO managerDTO) throws ParseException {
        boolean isSuccess = managerService.updateManagerInfo(managerDTO);
        int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
        String message = isSuccess == true ? "Cập nhật thông tin manager thành công" : "Cập nhật thông tin manager thất bại";
        return toResponse(new MessageResponse(status, message, null));
    }

    @GetMapping("/id/{managerId}")
    public ResponseEntity<?> getManagerInfoByManagerId(@PathVariable("managerId") Integer managerId) throws ParseException {
        ManagerDTO managerDTO = managerService.getManagerDetailByManagerId(managerId);
        int status = managerDTO == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = managerDTO == null ? "Lấy thông tin manager thất bại" : "Lấy thông tin manager thành công";
        return toResponse(new MessageResponse(status, message, managerDTO));
    }

    @GetMapping("/salon/{salonId}")
    public ResponseEntity<?> getManagerInfoBySalonId(@PathVariable("salonId") Integer salonId) throws ParseException {
        ManagerDTO managerDTO = managerService.getManagerDetailBySalonId(salonId);
        int status = managerDTO == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = managerDTO == null ? "Lấy thông tin manager thất bại" : "Lấy thông tin manager thành công";
        return toResponse(new MessageResponse(status, message, managerDTO));
    }

}
