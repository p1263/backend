package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.entity.RoleFunction;
import com.fpt.edu.projectthesis.model.*;
import com.fpt.edu.projectthesis.repository.RoleFunctionRepository;
import com.fpt.edu.projectthesis.service.*;
import org.aspectj.bridge.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.FileTypeMap;
import javax.persistence.EntityManager;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;
import java.util.HashMap;

@Service
@RequestMapping("/images")
public class ImageController extends BaseController {

    @GetMapping("/app-icons/{roleId}/{imageName}")
    public ResponseEntity<byte[]> getImageByRole(@PathVariable("roleId") Integer roleId, @PathVariable("imageName") String imageName) throws IOException {
        String pathname = "app-icons/" + roleId + "/" + imageName;
        File img = new File(pathname);
        return ResponseEntity.ok().contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img))).body(Files.readAllBytes(img.toPath()));
    }

    @GetMapping("/services/{imageName}")
    public ResponseEntity<byte[]> getServiceImage(@PathVariable("imageName")String imageName) throws IOException{
        String pathName = "services/"+imageName;
        File img = new File(pathName);
        return ResponseEntity.ok().contentType(MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType(img))).body((Files.readAllBytes(img.toPath())));
    }



}
