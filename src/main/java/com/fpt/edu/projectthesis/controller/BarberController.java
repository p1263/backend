package com.fpt.edu.projectthesis.controller;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.model.BarberDTO;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.service.BarberService;
import com.fpt.edu.projectthesis.service.SalonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/api/barber")
public class BarberController extends BaseController {
    @Autowired
    private BarberService barberService;

    @Autowired
    private SalonService salonService;

    @PostMapping("/updateInfo")
    public ResponseEntity<?> updateBarberByBarberId(@RequestBody BarberDTO barberDTO) throws ParseException {
        if (barberDTO.getName() != null && !barberDTO.getName().isEmpty()) {
            if (barberDTO.getCMND() != null && !barberDTO.getCMND().isEmpty()) {
                if (barberDTO.getEmail() != null && !barberDTO.getEmail().isEmpty()) {
                    if (barberDTO.getAvatar() != null && !barberDTO.getAvatar().isEmpty()) {
                        if (barberDTO.getDOB() != null) {
                            BarberDTO barberDTOResult = barberService.updateBarberInfo(barberDTO);
                            int status = barberDTOResult == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
                            String message = barberDTOResult == null ? "Cập nhật thông tin thất bại" : "Cập nhật thông tin thành công";
                            return toResponse(new MessageResponse(status, message, barberDTOResult));
                        }
                        return toResponse(new MessageResponse(ResponseStatus.FAIL, "DOB không được để trống", null));
                    }
                    return toResponse(new MessageResponse(ResponseStatus.FAIL, "Ảnh không được để trống", null));
                }
                return toResponse(new MessageResponse(ResponseStatus.FAIL, "Email không được trống", null));
            }
            return toResponse(new MessageResponse(ResponseStatus.FAIL, "Chứng minh nhân dân không được để trống", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Tên không được để trống", null));
    }

    @GetMapping("/salon/{salonId}")
    public ResponseEntity<?> getBarberBySalonId(@PathVariable("salonId") Integer salonId) {
        if(salonService.getSalonById(salonId) == null) return toResponse(new MessageResponse(ResponseStatus.FAIL,"Salon không tồn tại", null));
        Object obj = barberService.getBarberBySalonId(salonId);
        int status = ResponseStatus.SUCCESS;
        String message = "Lấy danh sách Barber thành công";
        return toResponse(new MessageResponse(status, message, obj));
    }

    @GetMapping("/{phone}")
    public ResponseEntity<?> getBarberByPhone(@PathVariable("phone") String phone) {
        Object obj = barberService.getBarberByPhone(phone);

        int status = ResponseStatus.SUCCESS;
        String message;
        if (obj == null) {
            status = ResponseStatus.FAIL;
            message = "Không có dữ liệu";
        } else {
            message = "Lấy dữ liệu Barber thành công";
        }
        return toResponse(new MessageResponse(status, message, obj));
    }


    @PostMapping("/add")
    public ResponseEntity<?> addNewBarber(@RequestBody ObjectNode json) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date1 = json.get("dob").asText();
        Date date = formatter.parse(date1);
        BarberDTO barberDTO = new BarberDTO();
        barberDTO.setName(json.get("name").asText());
        barberDTO.setCMND(json.get("CMND").asText());
        barberDTO.setEmail(json.get("email").asText());
        barberDTO.setDOB(date);
        barberDTO.setRoleId(Integer.parseInt(json.get("roleId").asText()));
        barberDTO.setPhone(json.get("phone").asText());
        barberDTO.setAvatar(json.get("avatar").asText());
        barberDTO.setSalonId(Integer.parseInt(json.get("salonId").asText()));
        String result = barberService.addNewBarber(barberDTO);
        if(result == null){
            return toResponse(new MessageResponse(ResponseStatus.FAIL,"Thêm mới barber thất bại - Barber đã tồn tại",null));
        }else if(result.equals("0")){
            return toResponse(new MessageResponse(ResponseStatus.FAIL,"Thêm mới barber thất bại",null));
        }
        return toResponse(new MessageResponse(ResponseStatus.SUCCESS, result, null));
    }

}
