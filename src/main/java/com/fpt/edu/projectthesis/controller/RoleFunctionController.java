package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.entity.RoleFunction;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.repository.RoleFunctionRepository;
import com.fpt.edu.projectthesis.service.FileUploadUtilService;
import com.fpt.edu.projectthesis.service.RoleFunctionService;
import com.fpt.edu.projectthesis.service.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@ControllerAdvice
@RequestMapping("/api/function")
public class RoleFunctionController extends BaseController {

    @Autowired
    private RoleFunctionRepository roleFunctionRepository;

    @Autowired
    private ValidateService validateService;

    @Autowired
    private FileUploadUtilService fileUploadUtil;

    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<?> handleFileUploadError(){
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Ảnh tải lên không được lớn hơn 10MB", null));
    }

    @PostMapping(value = "/insert", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<?> insertAppIcons(@RequestParam("roleId") String roleId,
                                            @RequestParam("functionName") String functionName,
                                            @RequestParam("icon") MultipartFile icon) throws IOException {
        if (validateService.isValidNumber(roleId)) {
            if (Integer.parseInt(roleId) > 0 && Integer.parseInt(roleId) < 5){
                if (functionName.isEmpty() || functionName == "" || functionName == null)
                    return toResponse(new MessageResponse(ResponseStatus.FAIL, "Tên Function không được rỗng", null));
                if (icon != null && !icon.isEmpty()) {
                    if(icon.getSize() >= 10485760) return toResponse(new MessageResponse(ResponseStatus.FAIL,"Ảnh không được quá 10MB", null));
                    RoleFunction roleFunction = new RoleFunction();
                    roleFunction.setFunctionName(functionName);
                    roleFunction.setRoleId(Integer.parseInt(roleId));
                    String iconFileName = StringUtils.cleanPath(icon.getOriginalFilename());
                    String uploadDir = "app-icons/"+roleId;
                    String iconNameToDB = "images/"+uploadDir+"/"+iconFileName;
                    roleFunction.setIcon(iconNameToDB);
                    roleFunctionRepository.save(roleFunction);
                    fileUploadUtil.saveFile(uploadDir,iconFileName,icon);
                    return toResponse(new MessageResponse(ResponseStatus.SUCCESS,"Thêm mới function thành công",null));
                } return toResponse(new MessageResponse(ResponseStatus.FAIL, "Icon không được rỗng", null));
            } return toResponse(new MessageResponse(ResponseStatus.FAIL, "Role phải lớn hơn 0 và nhỏ hơn 5", null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Sai format RoleID", null));

    }
}
