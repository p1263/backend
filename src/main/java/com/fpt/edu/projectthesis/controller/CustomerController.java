package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.model.CustomerDTO;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.text.ParseException;

@Controller
@RequestMapping("/api/customer")
public class CustomerController extends BaseController {

    @Autowired
    private CustomerService customerService;


    @PostMapping("/details")
    public ResponseEntity<?> getCustomerDetail(@RequestBody CustomerDTO customerDTO) throws ParseException {
        Object obj  = customerService.getCustomerDetailByPhone(customerDTO.getPhone());
        if(obj == null){
            int status = ResponseStatus.FAIL;
            String message = "Không có dữ liệu";
            return toResponse(new MessageResponse(status,message,obj));
        }else return toResponse(new MessageResponse(ResponseStatus.SUCCESS,"Call API Successfully", obj));
    }

    @PostMapping("/update")
    public ResponseEntity<?> updateCustomer(@RequestBody CustomerDTO customerDTO) throws ParseException {
        boolean isSuccess = customerService.updateCustomerInfo(customerDTO);
        int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
        String message = isSuccess ? "Cập nhật thông tin khách hàng thành công" : "Cập nhật thông tin khách hàng thất bại";
        return toResponse(new MessageResponse(status, message, null));
    }

}
