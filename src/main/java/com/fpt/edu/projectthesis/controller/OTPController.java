package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.entity.OTPTemp;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.model.OTPTempDTO;
import com.fpt.edu.projectthesis.service.OTPService;
import com.fpt.edu.projectthesis.service.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;

@Controller
@RequestMapping("/api/otp")
public class OTPController extends BaseController {

    @Autowired
    private ValidateService validateService;

    @Autowired
    private OTPService otpService;

    @PostMapping("/check")
    public ResponseEntity<?> checkOTP(@RequestBody OTPTempDTO otpTempDTO) {
        if (validateService.isValidPhoneAndNotNull(otpTempDTO.getPhone())) {
            boolean isSuccess = otpService.checkEqualsOTP(otpTempDTO.getPhone(), otpTempDTO.getOtpCode());
            int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
            String message = isSuccess ? "Kiểm tra OTP thành công" : "Sai mã OTP, vui lòng nhập lại";
            return toResponse(new MessageResponse(status, message, null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không hợp lệ", null));
    }

    @PostMapping("/send")
    public ResponseEntity<?> sendOTP(@RequestBody OTPTempDTO otpTempDTO) throws IOException {
        if (validateService.isValidPhoneAndNotNull(otpTempDTO.getPhone())) {
            String message = otpService.sendGetJSON(otpTempDTO.getPhone());
            int status = message.equals("SUCCESS") ? 0 : 1;
            return toResponse(new MessageResponse(status, message, null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không hợp lệ", null));
    }

    @PostMapping("/resend")
    public ResponseEntity<?> reSendOTP(@RequestBody OTPTempDTO otpTempDTO) throws IOException {
        if (validateService.isValidPhoneAndNotNull(otpTempDTO.getPhone())) {
            String message = otpService.resendOTPJSON(otpTempDTO.getPhone());
            return toResponse(new MessageResponse(ResponseStatus.SUCCESS, message, null));
        }
        return toResponse(new MessageResponse(ResponseStatus.FAIL, "Số điện thoại không hợp lệ", null));
    }


}
