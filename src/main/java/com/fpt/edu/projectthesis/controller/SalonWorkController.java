package com.fpt.edu.projectthesis.controller;

import com.fpt.edu.projectthesis.constant.ResponseStatus;
import com.fpt.edu.projectthesis.model.MessageResponse;
import com.fpt.edu.projectthesis.model.SalonWorkDTO;
import com.fpt.edu.projectthesis.service.SalonWorkService;
import com.fpt.edu.projectthesis.service.ValidateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@Service
@RequestMapping("/api/service")
public class SalonWorkController extends BaseController {

    @Autowired
    private SalonWorkService salonWorkService;

    @GetMapping("/getAll")
    public ResponseEntity<?> getAllSalonService() {
        List<SalonWorkDTO> listSalonWorkDTO = salonWorkService.getAllSalonWork();
        int status = ResponseStatus.SUCCESS;
        String message = listSalonWorkDTO.isEmpty() ? "Không có dữ liệu" : "Lấy danh sách dịch vụ thành công";
        return toResponse(new MessageResponse(status, message, listSalonWorkDTO));
    }

    @PostMapping("/detail")
    public ResponseEntity<?> getSalonServiceById(@RequestBody SalonWorkDTO salonWorkDTO) {
        Object data = salonWorkService.getSalonWorkById(salonWorkDTO.getServiceId());
        int status = ResponseStatus.SUCCESS;
        String message = data != null ? "Lấy thông tin dịch vụ thành công" : "Lấy thông tin dịch vụ thất bại - Không tìm thấy trong DB";
        return toResponse(new MessageResponse(status, message, data));
    }

    @PostMapping(value = "/create")
    public ResponseEntity<?> createSalonService(@RequestBody SalonWorkDTO salonWorkDTO) {
        SalonWorkDTO salonWorkDTOResult = salonWorkService.createSalonService(salonWorkDTO);
        int status = salonWorkDTOResult == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = salonWorkDTOResult == null ?  "Khởi tạo dịch vụ thất bại" : "Khởi tạo dịch vụ thành công";
        return toResponse(new MessageResponse(status,message,salonWorkDTOResult));
    }

    @PostMapping(value = "/update")
    public ResponseEntity<?> updateSalonService(@RequestBody SalonWorkDTO salonWorkDTO)  {
        SalonWorkDTO salonWorkDTOResult = salonWorkService.updateSalonService(salonWorkDTO);
        int status = salonWorkDTOResult == null ? ResponseStatus.FAIL : ResponseStatus.SUCCESS;
        String message = salonWorkDTOResult == null ?  "Cập nhật dịch vụ thất bại" : "Cập nhật dịch vụ thành công";
        return toResponse(new MessageResponse(status,message,salonWorkDTOResult));
    }

    @PostMapping("/delete")
    public ResponseEntity<?> deleteSalonService(@RequestBody SalonWorkDTO salonWorkDTO){
        boolean isSuccess = salonWorkService.deleteSalonService(salonWorkDTO.getServiceId());
        int status = isSuccess == true ? ResponseStatus.SUCCESS : ResponseStatus.FAIL;
        String message = isSuccess == true ? "Xóa dịch vụ thành công" : "Xóa dịch vụ thất bại";
        return toResponse(new MessageResponse(status,message,null));
    }


}
