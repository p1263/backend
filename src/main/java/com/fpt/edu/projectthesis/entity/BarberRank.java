package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "barber_rank")
public class BarberRank {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "rank_name")
    private String ranks;
    @Column(name = "rank_point")
    private Double rankPoint;
    @Column (name = "base_point")
    private Double basePoint;
    @Column(name = "number_of_customer_assigned")
    private Integer noca;
    @Column
    private Double multiplier;
}

