package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "role_function")
public class RoleFunction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column(name="role_id")
    private Integer roleId;

    @Column(name="function_code")
    private String functionCode;

    @Column (name="function_name")
    private String functionName;

    @Column
    private String icon;


}
