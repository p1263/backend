package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "history")
@NoArgsConstructor
@AllArgsConstructor
public class History {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private Integer customer_id;
    @Column
    private Integer barber_id;
    @Column
    private Double price;
    @Column
    private Integer booking_id;
    @Column
    private Integer salon_id;
    @Column
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
    private Date date;
    @Column
    private Integer star;
    @Column
    private String frontPic;
    @Column
    private String leftPic;
    @Column
    private String behindPic;
    @Column
    private String rightPic;

}
