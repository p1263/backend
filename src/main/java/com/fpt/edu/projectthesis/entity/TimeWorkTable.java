package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "time_work_table")
public class TimeWorkTable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "time_id")
    private Integer timeId;
    @Column(name = "time_stamp")
    @Temporal(TemporalType.TIME)
    @DateTimeFormat(pattern = "HH:mm:ss")
    private Date timeStamp;

}
