package com.fpt.edu.projectthesis.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="salon")
public class Salon {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "salon_id")
    private Integer salonId;
    @Column
    private String name;
    @Column
    private String address;
    @Column(name = "manager_id")
    private Integer managerId;
    @Column
    private Double latLocation;
    @Column
    private Double lngLocation;
    @Column
    private String avatar;
}
