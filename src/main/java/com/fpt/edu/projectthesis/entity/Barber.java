package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="barber")
public class Barber {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "barber_id")
    private Integer barberId;
    @Column
    private String name;
    @Column
    private String CMND;
    @Column
    private String email;
    @Column
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "DD/MM/YYYY")
    private Date DOB;
    @Column(name = "phone_number")
    private String phone;
    @Column
    private String avatar;
    @Column(name = "salon_id")
    private Integer salonId;
    @Column(name = "role_id")
    private Integer roleId;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy/DD/mm")
    @Column(name = "day_enter")
    private Date dayEnter;
    @Column(name = "base_point")
    private Double basePoint;
    @Column(name = "rank_point")
    private Double rankPoint;
    @Column(name = "rank_id")
    private Integer rankId;


    public Barber(String name, String CMND, String email, Date DOB, String phone, String avatar, Integer salonId) {
        this.name = name;
        this.CMND = CMND;
        this.email = email;
        this.DOB = DOB;
        this.phone = phone;
        this.avatar = avatar;
        this.salonId = salonId;
        this.basePoint=350.0;
        this.rankId=4;
        this.rankPoint=500.0;
        this.dayEnter = new Date();
    }



}
