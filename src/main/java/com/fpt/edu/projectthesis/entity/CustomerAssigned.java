package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "customer_assigned")
public class CustomerAssigned {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer Id;
    @Column(name = "barber_id")
    private Integer barberId;
    @Column(name = "salon_id")
    private Integer salonId;
    @Column(name = "number_of_customer_assigned")
    private Integer noca;
}
