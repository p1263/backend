package com.fpt.edu.projectthesis.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "customer")
public class Customer {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dob;
    @Column
    private String email;
    @Column
    private String avatar;
    @Column
    private String address;
    @Column(name = "phone_number")
    private String phone;
    @Column(name = "role_id")
    private int roleId;
    @Column
    private Double total_purchase;
    @Column
    private Integer rank_id;
}
