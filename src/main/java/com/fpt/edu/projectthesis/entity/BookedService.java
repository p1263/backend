package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "booking_service")
public class BookedService {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "booking_id")
    private Integer bookingId;
    @Column(name = "service_id")
    private Integer serviceId;

    public BookedService(Integer bookingId, Integer serviceId) {
        this.bookingId = bookingId;
        this.serviceId = serviceId;
    }
}
