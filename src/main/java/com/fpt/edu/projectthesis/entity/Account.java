package com.fpt.edu.projectthesis.entity;


import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="account")
public class Account {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column
    private Integer id;
    @Column
    private String phone;
    @Column
    private String password;
    @Column(name = "role_id")
    private Integer roleId;
    @Column(name="token")
    private String deviceToken;
}
