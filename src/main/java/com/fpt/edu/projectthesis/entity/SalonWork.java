package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "salonservice")
public class SalonWork {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "service_id")
    private Integer serviceId;
    @Column
    private String name;
    @Column
    private Double price;
    @Column
    private String description;
    @Column
    private String image;
    @Column
    private Integer point;
    @Column
    private boolean status;

}
