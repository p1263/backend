package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "booking")
public class Booking {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "booking_id")
    private Integer bookingId;
    @Column(name = "customer_id")
    private Integer customerId;
    @Column(name = "salon_id")
    private Integer salonId;
    @Column(name = "barber_id")
    private Integer barberId;
    @Column(name = "time_id")
    private Integer timeId;
    @Column
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    @Column
    private Integer star;
    @Column
    private String status;
    @Column
    private String frontPic;
    @Column
    private String leftPic;
    @Column
    private String behindPic;
    @Column
    private String rightPic;


    public Booking(Integer customerId, Integer salonId, Integer barberId, Integer timeId, Date date) {
        this.customerId = customerId;
        this.salonId = salonId;
        this.barberId = barberId;
        this.timeId = timeId;
        this.date = date;
        star = 0;

    }


}
