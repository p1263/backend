package com.fpt.edu.projectthesis.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "customer_rank")
public class CustomerRank {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "rank_name")
    private String rankName;
    @Column(name = "money_spent")
    private Integer money;
    @Column
    private Double multiplier;
}
