package com.fpt.edu.projectthesis.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "barber_work_table")
public class BarberWorkTable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "barber_id")
    private Integer barberId;
    @Column(name = "booking_id")
    private Integer bookingId;
    @Column(name = "time_id")
    private Integer timeId;
    @Column
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date date;
    @Column
    private boolean status;
    @Column(name = "salon_id")
    private Integer salonId;

}
