package com.fpt.edu.projectthesis.repository;


import com.fpt.edu.projectthesis.entity.BarberRank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BarberRankRepository extends JpaRepository<BarberRank,Integer> {

    @Query("SELECT b.noca FROM BarberRank b WHERE b.id =:id")
    Integer getRankById(@Param("id") Integer id);

    @Query("SELECT b.basePoint FROM BarberRank b WHERE b.id =:id")
    Double getBarberBaseRank(@Param("id") Integer id);
    @Query("SELECT b FROM BarberRank b WHERE b.id=:id")
    BarberRank getBarberRankByBarberId(@Param("id") Integer id);
}
