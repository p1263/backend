package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.OTPTemp;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OTPTempRepository extends JpaRepository<OTPTemp, Integer> {

    Optional<OTPTemp> findOTPTempByPhone(String phone);

    @Query("SELECT o from OTPTemp o WHERE o.otpcode IS NOT NULL AND o.otpcode <> ''")
    List<OTPTemp> findOTPTempsNotNullAndNotBlank();

}
