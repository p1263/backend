package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Rate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RateRepository extends JpaRepository<Rate,Integer> {
    @Query("SELECT r FROM Rate r WHERE r.id=:id")
    Rate getRateById(@Param("id") Integer id);
}
