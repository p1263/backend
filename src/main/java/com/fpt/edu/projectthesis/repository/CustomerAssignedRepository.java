package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.CustomerAssigned;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface CustomerAssignedRepository  extends JpaRepository<CustomerAssigned,Integer> {

    @Query("SELECT c FROM CustomerAssigned c WHERE c.barberId=:barberId")
    CustomerAssigned getByBarberId(@Param("barberId") Integer barberId);

    @Query("SELECT c.noca FROM CustomerAssigned c where c.barberId=:barberId")
    Integer getNumberAssigned(@Param("barberId") Integer barberId);

    @Query("SELECT sum(c.noca) FROM CustomerAssigned c WHERE c.salonId=:id")
    Integer getSumRandomList(@Param("id") Integer salonId);

    @Query("SELECT c FROM CustomerAssigned c WHERE c.salonId=:id")
    List<CustomerAssigned> getListbySalonId(@Param("id") Integer salonId);
}
