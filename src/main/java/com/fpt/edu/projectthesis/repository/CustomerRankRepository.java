package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.CustomerRank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRankRepository extends JpaRepository<CustomerRank,Integer> {
    @Query("SELECT c FROM CustomerRank c WHERE c.id=:id")
    CustomerRank getRankById(@Param("id") Integer id);
}
