package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.TimeWorkTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface TimeWorkTableRepository extends JpaRepository<TimeWorkTable, Integer> {

    @Query("SELECT t FROM TimeWorkTable t")
    List<TimeWorkTable> getAllTimeWorkTable();
    @Query("SELECT  t.timeStamp FROM TimeWorkTable t WHERE t.timeId=:id")
    Date getTimebyId(@Param("id") Integer id);

}
