package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.SalonWork;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface SalonWorkRepository extends JpaRepository<SalonWork, Integer> {

    @Query("Select s from SalonWork s where s.serviceId=:serviceId")
    Optional<SalonWork> findSalonWorkByServiceId(@Param("serviceId") int serviceId);

    @Query("SELECT s.description FROM SalonWork s WHERE s.serviceId=:serviceId")
    String getSalonWorkNameByServiceId(@Param("serviceId") Integer serviceId);

    @Query("SELECT s FROM SalonWork s WHERE s.status=TRUE")
    List<SalonWork> findAllSalonWorkByStatusTrue();

    @Modifying
    @Query("UPDATE SalonWork s SET s.status=FALSE WHERE s.serviceId=:serviceId")
    Integer deleteSalonWorkByServicesId(@Param("serviceId")Integer serviceId);
}
