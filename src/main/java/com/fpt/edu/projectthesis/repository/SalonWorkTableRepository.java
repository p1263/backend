package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.SalonWorkTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface SalonWorkTableRepository extends JpaRepository<SalonWorkTable,Integer> {

    List<SalonWorkTable> getSalonWorkTableByDateBefore(Date today);

    @Query("SELECT min(s.date) FROM SalonWorkTable s")
    Date getMinSalonWorktabledate();

    @Query("SELECT s FROM SalonWorkTable s WHERE s.salonId=:salonId ORDER BY s.date,s.timeId")
    List<SalonWorkTable> getSalonWorkTableBySalonId(@Param("salonId") Integer salonId);

    @Query("SELECT s FROM SalonWorkTable s WHERE s.salonId=:salonId AND s.timeId=:timeId AND s.date=:date ")
    SalonWorkTable getSalonWorkTableToUpdate(@Param("salonId") Integer salonId, @Param("timeId") Integer timeId, @Param("date") Date date);

    @Query("SELECT s FROM SalonWorkTable s WHERE s.timeId=:timeId AND s.date=:date")
    List<SalonWorkTable> getSalonWorkTableByTimeId(@Param("timeId") Integer timeId,@Param("date") Date date);
}
