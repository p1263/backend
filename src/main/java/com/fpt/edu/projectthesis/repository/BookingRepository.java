package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface BookingRepository extends JpaRepository<Booking, Integer> {

    @Query("select b from Booking b ")
    List<Booking> getAllBooking();

    Optional<Booking> getBookingByBookingId(Integer id);

    @Query("SELECT b.salonId FROM Booking b WHERE b.bookingId=:bookingId")
    Integer getSalonIdByBookingId(@Param("bookingId") Integer bookingId);

    @Query("SELECT b FROM Booking b WHERE b.salonId=:salon_id AND b.date =:date ")
    List<Booking> getBookingBySalonIdAndDate(@Param("salon_id") Integer salonId, @Param("date")Date date);

    //Get Booking By CustomerPhone
    @Query("SELECT b FROM Booking b WHERE b.customerId = (SELECT c.id FROM Customer c WHERE c.phone =:phone)")
    Optional<Booking> getLastestBookingByCustomerPhone(@Param("phone")String phone);

    @Query("SELECT b FROM Booking b WHERE b.bookingId=:bookingId")
    Booking getBookedByBookingId(@Param("bookingId") Integer bookingId);

    @Query("SELECT b FROM Booking b WHERE b.customerId=:customerId AND (b.status='Waiting' OR b.status='Arrived' OR b.status='Serving' OR b.status='Billing')")
    Booking getBookedByCustomerId(@Param("customerId") Integer customerId);

    @Query("SELECT b FROM Booking b WHERE b.customerId=:customerId AND b.salonId=:salonId AND b.status='Waiting'")
    Optional<Booking> getBookingBySalonIdAndDateAndCustomerId(@Param("customerId") Integer customerId, @Param("salonId")Integer salonId);

    @Query("SELECT b FROM Booking b WHERE b.salonId=:salonId AND b.status='Waiting'")
    List<Booking> getListBookingBySalonIdAndStatusWait(@Param("salonId")Integer salonId);

    @Query("SELECT b FROM Booking b WHERE b.status='Waiting' AND b.timeId=:timeId AND b.date=:date")
    List<Booking> getBookingToAutoCancel(@Param("timeId") Integer timeId, @Param("date") Date date);

    List<Booking> getBookingsByDateBeforeAndStatus(Date today, String status);

    @Query("SELECT b FROM Booking b WHERE b.status = 'Billing' AND b.salonId=:salonId")
    List<Booking> getListBillingBySalonId(@Param("salonId") Integer salonId);

    @Query(nativeQuery = true,value = "SELECT * FROM booking b WHERE b.status LIKE :status  AND b.barber_id=:barberId ORDER BY booking_id DESC LIMIT :page,5 ")
    List<Booking> getListWorkOfDayForBarber(@Param("barberId") Integer barberId, @Param("page") int page,@Param("status") String status);

    //AND b.status NOT IN('Completed','Cancelled')
    @Query(nativeQuery = true,value = "SELECT * FROM booking b WHERE b.salon_id=:salonId ORDER BY booking_id DESC LIMIT :page,5")
    List<Booking> findBookingsBySalonId(@Param("salonId") Integer salonId,@Param("page") int page);

    @Query(nativeQuery = true,value = "SELECT * FROM booking b WHERE b.salon_id=:salonId AND b.status=:status ORDER BY booking_id DESC LIMIT :page,5")
    List<Booking> findBookingsBySalonIdAndStatus(@Param("salonId") Integer salonId, @Param("status")String status,@Param("page") int page);

}
