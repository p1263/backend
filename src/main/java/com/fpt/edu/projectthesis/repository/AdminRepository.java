package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface AdminRepository extends JpaRepository<Admin, Integer> {

    Optional<Admin> findAdminByPhone(String phone);

}
