package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.RoleFunction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface RoleFunctionRepository extends JpaRepository<RoleFunction, Integer> {

    List<RoleFunction> findAllByRoleId(Integer roleId);

}
