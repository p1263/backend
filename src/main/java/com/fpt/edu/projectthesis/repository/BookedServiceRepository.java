package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.BookedService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface BookedServiceRepository extends JpaRepository<BookedService,Integer> {

    Optional<List<BookedService>> getAllByBookingId(Integer bookingId);

    List<BookedService> findBookedServiceByBookingId(Integer bookingId);

    void deleteBookedServiceByBookingId(Integer bookingId);

    @Modifying
    @Query("DELETE FROM BookedService b WHERE b.bookingId=:bookingId AND b.serviceId=:serviceId")
    void deleteBookedServiceByBookingIdAndBookedServiceId(@Param("bookingId")Integer bookingId, @Param("serviceId")Integer serviceId);
}
