package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.History;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface HistoryRepositoty extends JpaRepository<History, Integer> {
    @Query("SELECT h FROM History h WHERE h.customer_id= :customer_id ORDER BY h.booking_id DESC")
    List<History> findHistoryByCustomerId(@Param("customer_id")Integer customer_id);

    @Query(value = "SELECT * FROM History h WHERE h.customer_id=:customer_id ORDER BY h.id DESC LIMIT 0,1 ",nativeQuery = true)
    Optional<History> getLatestHistoryByCustomerId(@Param("customer_id") Integer customerId);

    @Query("SELECT h FROM History h WHERE h.booking_id=:bookingId")
    Optional<History> getHistoryByByBookingId(@Param("bookingId")Integer bookingId);

    @Modifying
    @Query("UPDATE History h SET h.star=:star WHERE h.booking_id=:bookingId")
    Integer updateStarOfHistoryByBookingId(@Param("star")Integer star, @Param("bookingId") Integer bookingId);
}
