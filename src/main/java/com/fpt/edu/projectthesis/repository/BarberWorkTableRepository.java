package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.BarberWorkTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface BarberWorkTableRepository extends JpaRepository<BarberWorkTable,Integer> {


    List<BarberWorkTable>getWorkTablesByDateBefore(Date today);

    @Query("SELECT w FROM BarberWorkTable w WHERE w.barberId =:barber_id ORDER BY w.date,w.timeId")
    List<BarberWorkTable> getWorkTablesByBarberId(@Param("barber_id") Integer barber_id);

    List<BarberWorkTable> getWorkTablesByDate(Date today);

    @Query("SELECT MIN (w.date) FROM BarberWorkTable w")
    Date minWorktableDate();

    @Query("SELECT w FROM BarberWorkTable w")
    List<BarberWorkTable> getListBarberWorktable();

    @Query("SELECT w.barberId FROM BarberWorkTable w WHERE w.salonId=:salonId AND w.timeId=:timeId AND w.date=:date AND w.status=true ")
    List<Integer> getFreeBarberId(@Param("salonId") Integer salonId, @Param("timeId") Integer timeId, @Param("date") Date date );

    @Query("SELECT w.barberId FROM BarberWorkTable w WHERE w.salonId=:salonId AND w.timeId=:timeId AND w.date=:date AND w.bookingId IS NULL ")
    List<Integer> getFreeBarberIdForManager(@Param("salonId") Integer salonId, @Param("timeId") Integer timeId, @Param("date") Date date);

    @Query("SELECT w FROM BarberWorkTable w WHERE w.barberId=:barberId AND w.timeId=:timeId AND w.date=:date")
    BarberWorkTable getBarberWorkTableToUpdate(@Param("barberId") Integer barberId, @Param("timeId") Integer timeId, @Param("date") Date date);

    @Query("SELECT w FROM BarberWorkTable w WHERE w.timeId=:timeId AND w.date=:date")
    List<BarberWorkTable> getBarberWorkTableByTimeId(@Param("timeId") Integer timeId,@Param("date")Date date);


}
