package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Manager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface ManagerRepository extends JpaRepository<Manager, Integer> {

    Optional<Manager> findManagerByPhone(String phone);

    Optional<Manager> findManagerBySalonId(Integer salonId);
}
