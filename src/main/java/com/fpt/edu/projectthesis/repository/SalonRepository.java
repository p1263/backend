package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Salon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface SalonRepository extends JpaRepository<Salon, Integer> {

    @Query("SELECT s FROM Salon s")
    List<Salon> getAllSalon();

    @Query("SELECT s.name FROM Salon s WHERE s.salonId=:salonId")
    String getSalonNameBySalonId(@Param("salonId") Integer salonId);

    @Query("SELECT s FROM Salon s WHERE s.latLocation=:lat AND s.lngLocation=:lng")
    List<Salon> getSalonByLocation(@Param("lat") Double lat, @Param("lng") Double lng);

    @Query("SELECT s FROM Salon s WHERE s.managerId IS NULL")
    List<Salon> getListSalonFree();
}
