package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role getRoleById(int id);

    @Query("SELECT r FROM Role r WHERE r.id <3")
    List<Role> getRoleCustomerAndBarber();
}
