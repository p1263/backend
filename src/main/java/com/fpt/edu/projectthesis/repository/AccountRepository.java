package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface AccountRepository extends JpaRepository<Account, Integer> {



    @Modifying
    @Query(value = "UPDATE Account a SET a.password= :password WHERE a.phone= :phone ")
    Integer changePassword(@Param("password") String password, @Param("phone") String phone);


    Optional<Account> findAccountByPhone(String phone);
    @Modifying
    @Query(value = "UPDATE Account a SET a.roleId=:roleId WHERE a.phone=:phone")
    Integer changeRoleAccount(@Param("roleId") Integer roleId, @Param("phone") String phone);

    @Query(value = "SELECT a.roleId FROM Account a WHERE a.phone=:phone")
    Integer getRoleIdOfAccountByPhone(@Param("phone") String phone);

    @Query("SELECT a.deviceToken FROM Account a WHERE a.phone=:phone")
    String getDeviceTokenByPhone(String phone);

    @Modifying
    @Query("UPDATE Account a SET a.deviceToken='' WHERE a.phone=:phone")
    Integer removeDeviceTokenFromAccount(@Param("phone")String phone);
}
