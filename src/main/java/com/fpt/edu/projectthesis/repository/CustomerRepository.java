package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;

@Repository
@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Optional<Customer> findCustomerByPhone(String phone);

    @Query("SELECT c.rank_id FROM Customer c WHERE c.id=:id")
    int getCustomerRankById(@Param("id") Integer id);

    Optional<Customer> findCustomerById(Integer id);

    @Query("SELECT c.name FROM Customer c WHERE c.id=:id")
    String getCustomerNameById(@Param("id") Integer id);

    @Query("SELECT c FROM Customer c WHERE c.phone=:phone")
    Customer getCustomerByPhone(@Param("phone") String phone);
}
