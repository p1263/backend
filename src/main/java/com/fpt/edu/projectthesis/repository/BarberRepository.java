package com.fpt.edu.projectthesis.repository;

import com.fpt.edu.projectthesis.entity.Barber;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface BarberRepository extends JpaRepository<Barber,Integer> {

    @Query("SELECT b FROM Barber b WHERE b.salonId =:salonId")
    List<Barber> getBarbersBySalon_id(@Param("salonId") Integer salonId);

    @Query("SELECT b FROM Barber b WHERE b.barberId =:barberId")
    Barber getBarberByBarber_id(@Param("barberId") Integer barberId);

    @Query("SELECT b FROM Barber b")
    List<Barber> getAllBarber();

    @Query("SELECT b.barberId FROM Barber b")
    List<Integer> getAllBarberId();

    @Query("SELECT b FROM Barber b WHERE b.phone=:phone")
    Barber getBarberByPhone(String phone);

    @Query("SELECT b.name FROM Barber b WHERE b.barberId=:barber_id")
    String getBarberNameByBarberId(@Param("barber_id") Integer barber_id);

    @Query("SELECT b.rankPoint FROM Barber b WHERE b.barberId =:barber_id")
    Double getBarberRankPointById(@Param("barber_id") Integer barber_id);

    @Query("SELECT COUNT (b) FROM Barber b WHERE b.salonId=:salonId")
    Integer getNumberOfBarberBySalonId(@Param("salonId") Integer salonId);

    Optional<Barber> findBarberByPhone(String phone);

}
